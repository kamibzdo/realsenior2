package camt.se331.shoppingcart.entity;

import camt.se331.shoppingcart.dao.ApartmentDao;
import camt.se331.shoppingcart.dao.ChatDao;
import camt.se331.shoppingcart.dao.UserDao;
import camt.se331.shoppingcart.service.ApartmentServiceImpl;
import camt.se331.shoppingcart.service.ChatServiceImlp;
import camt.se331.shoppingcart.service.UserServiceImpl;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 25/10/2558.
 */
@RunWith(JUnitParamsRunner.class)
public class ApartmentTest {


    ApartmentDao apartmentDao = Mockito.mock(ApartmentDao.class);
    ApartmentServiceImpl apartmentService = new ApartmentServiceImpl(apartmentDao);
    List<Apartment> apartment = new ArrayList<>();

    @Test
    public void testGetApartment() {
//        Favorite a = new Favorite();
//        favorite.add(a);
        when(apartmentDao.getApartment()).thenReturn(apartment);
        assertThat(apartmentService.getApartment(), is(apartment));
        assertThat(apartmentService.getApartment(null), is(nullValue()));
    }

    @Test
    public void testGetCompareWithID() {
        Apartment apartment1 = new Apartment();
        when(apartmentDao.getApartment(1l)).thenReturn(apartment1);
        assertThat(apartmentService.getApartment(1l), is(apartment1));
        assertThat(apartmentService.getApartment(null), is(nullValue()));

    }

    @Test
    public void testAddCompare() {
        Apartment apartment2 = new Apartment();
        when(apartmentDao.addApartment(apartment2)).thenReturn(apartment2);
        assertThat(apartmentService.addApartment(apartment2), is(apartment2));
        assertThat(apartmentService.addApartment(null), is(nullValue()));
    }

    @Test
    public void testDeleteCompare() {
        Apartment c = new Apartment();
        apartment.add(c);
        when(apartmentService.getApartment(1L)).thenReturn(apartment.get(0));
        when(apartmentDao.deleteApartment(apartment.get(0))).thenReturn(apartment.get(0));
//        when(compare.getUser().getCompares().remove(compare)).thenReturn();
        assertThat(apartmentService.deleteApartment(1L), is(apartment.get(0)));
        assertThat(apartmentService.deleteApartment(null), is(nullValue()));

    }

    @Test
    public void testGetApartmentBySearch() {
//        Favorite a = new Favorite();
//        favorite.add(a);
        when(apartmentDao.getApartmentBySearch("", "", "", "", "", "", "", "")).thenReturn(apartment);
        assertThat(apartmentService.getApartmentBySearch("", "", "", "", "", "", "", ""), is(apartment));
        assertThat(apartmentService.getApartmentBySearch(null, null,null,null,null,null,null,null), is(empty()));
    }

//    @Test
//    public void testGetCompareWithroomID() {
////        Chat chat1 = new Chat();
//        when(chatDao.getChatByChatroomid(1l)).thenReturn(chat);
//        assertThat(chatService.getChatByChatroomid(1l), is(chat));
////        assertThat(chatService.getChatByChatroomid(null), is(nullValue()));
//
//    }
}
