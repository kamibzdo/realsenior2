package camt.se331.shoppingcart.entity;

import camt.se331.shoppingcart.dao.CompareDao;
import camt.se331.shoppingcart.dao.FavoriteDao;
import camt.se331.shoppingcart.service.CompareServiceImlp;
import camt.se331.shoppingcart.service.FavoriteServiceImpl;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 24/10/2558.
 */
@RunWith(JUnitParamsRunner.class)
public class CompareTest {

    CompareDao compareDao = Mockito.mock(CompareDao.class);
    CompareServiceImlp compareService = new CompareServiceImlp(compareDao);
    List<Compare> compare = new ArrayList<>();

    @Test
    public void testGetCompare() {
//        Favorite a = new Favorite();
//        favorite.add(a);
        when(compareDao.getCompare()).thenReturn(compare);
        assertThat(compareService.getCompare(), is(compare));
        assertThat(compareService.getCompare(null), is(nullValue()));
    }

    @Test
    public void testGetCompareWithID() {
        Compare compare1 = new Compare();
        when(compareDao.getCompare(1l)).thenReturn(compare1);
        assertThat(compareService.getCompare(1l), is(compare1));
        assertThat(compareService.getCompare(null), is(nullValue()));

    }

    @Test
    public void testAddCompare() {
        Compare compare = new Compare();
        when(compareDao.addCompare(compare)).thenReturn(compare);
        assertThat(compareService.addCompare(compare), is(compare));
        assertThat(compareService.addCompare(null), is(nullValue()));
    }

    @Test
    public void testDeleteCompare() {
        Compare c = new Compare();
        compare.add(c);
        when(compareService.getCompare(1L)).thenReturn(compare.get(0));
        when(compareDao.deleteCompare(compare.get(0))).thenReturn(compare.get(0));
//        when(compare.getUser().getCompares().remove(compare)).thenReturn();
        assertThat(compareService.deleteCompare(1L), is(compare.get(0)));
        assertThat(compareService.deleteCompare(null), is(nullValue()));
    }

    @Test
    public void testGetCompareByuserid(){


        when(compareDao.getCompareByUserId(1l)).thenReturn(compare);
        assertThat(compareService.getCompareByUserId(1l), is(compare));
        assertThat(compareService.getCompareByUserId(null), is(empty()));



    }

}
