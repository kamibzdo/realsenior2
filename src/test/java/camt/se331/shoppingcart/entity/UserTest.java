package camt.se331.shoppingcart.entity;

import camt.se331.shoppingcart.dao.ChatDao;
import camt.se331.shoppingcart.dao.ChatDaoImlp;
import camt.se331.shoppingcart.dao.DbUserDao;
import camt.se331.shoppingcart.dao.UserDao;
import camt.se331.shoppingcart.repository.RoleRepository;
import camt.se331.shoppingcart.service.UserServiceImpl;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.describedAs;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 25/10/2558.
 */
@RunWith(JUnitParamsRunner.class)
public class UserTest {

    DbUserDao userDao = Mockito.mock(DbUserDao.class);
    UserServiceImpl userService = new UserServiceImpl(userDao);
    List<User> user = new ArrayList<>();


    @Test
    public void testGetUser() {
//        Favorite a = new Favorite();
//        favorite.add(a);
        when(userDao.getUser()).thenReturn(user);
        assertThat(userService.getUser(), is(user));
        assertThat(userService.getUser(null), is(nullValue()));
    }

//    @Test
//    public void testAddUser() {
//        Role role = new Role();
//        User user = new User();
//        RoleRepository roleRepository = new JpaRepository<Role,Long>();
//        when(roleRepository.findByRoleName(user.getRole().getRoleName())).thenReturn(role);
//        User user2 = new User();
//        when(userDao.addUser(user)).thenReturn(user2);
//        assertThat(userService.addUser(user),is(user2));
//    }
//
//    @Test
//    public void testDeleteUser() {
//        Chat c = new Chat();
//        chat.add(c);
//        when(chatService.getChat(1L)).thenReturn(chat.get(0));
//        when(chatDao.deleteChat(chat.get(0))).thenReturn(chat.get(0));
////        when(compare.getUser().getCompares().remove(compare)).thenReturn();
//        assertThat(chatService.deleteChat(1L), is(chat.get(0)));
//        assertThat(chatService.deleteChat(null), is(nullValue()));
//
//    }

//    @Test
//    public void testGetCompareWithroomID() {
////        Chat chat1 = new Chat();
//        when(chatDao.getChatByChatroomid(1l)).thenReturn(chat);
//        assertThat(chatService.getChatByChatroomid(1l), is(chat));
////        assertThat(chatService.getChatByChatroomid(null), is(nullValue()));
//
//    }
//
}
