package camt.se331.shoppingcart.entity;

import camt.se331.shoppingcart.dao.ChatDao;
import camt.se331.shoppingcart.dao.CompareDao;
import camt.se331.shoppingcart.service.ChatServiceImlp;
import camt.se331.shoppingcart.service.CompareServiceImlp;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 24/10/2558.
 */
@RunWith(JUnitParamsRunner.class)
public class ChatTest {

    ChatDao chatDao = Mockito.mock(ChatDao.class);
    ChatServiceImlp chatService = new ChatServiceImlp(chatDao);
    List<Chat> chat = new ArrayList<>();

    @Test
    public void testGetCompare() {
//        Favorite a = new Favorite();
//        favorite.add(a);
        when(chatDao.getChat()).thenReturn(chat);
        assertThat(chatService.getChat(), is(chat));
        assertThat(chatService.getChat(null), is(nullValue()));
    }

    @Test
    public void testGetCompareWithID() {
        Chat chat1 = new Chat();
        when(chatDao.getChat(1l)).thenReturn(chat1);
        assertThat(chatService.getChat(1l), is(chat1));
        assertThat(chatService.getChat(null), is(nullValue()));

    }

    @Test
    public void testAddCompare() {
        Chat chat = new Chat();
        when(chatDao.addChat(chat)).thenReturn(chat);
        assertThat(chatService.addChat(chat), is(chat));
        assertThat(chatService.addChat(null), is(nullValue()));
    }

    @Test
    public void testDeleteCompare() {
        Chat c = new Chat();
        chat.add(c);
        when(chatService.getChat(1L)).thenReturn(chat.get(0));
        when(chatDao.deleteChat(chat.get(0))).thenReturn(chat.get(0));
//        when(compare.getUser().getCompares().remove(compare)).thenReturn();
        assertThat(chatService.deleteChat(1L), is(chat.get(0)));
        assertThat(chatService.deleteChat(null), is(nullValue()));

    }

    @Test
    public void testGetCompareWithroomID() {
//        Chat chat1 = new Chat();
        when(chatDao.getChatByChatroomid(1l)).thenReturn(chat);
        assertThat(chatService.getChatByChatroomid(1l), is(chat));
        assertThat(chatService.getChatByChatroomid(null), is(empty()));

    }


}
