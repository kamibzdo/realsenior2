package camt.se331.shoppingcart.entity;

import camt.se331.shoppingcart.dao.FavoriteDao;
import camt.se331.shoppingcart.dao.MessageDao;
import camt.se331.shoppingcart.service.FavoriteServiceImpl;
import camt.se331.shoppingcart.service.MessageServiceImlp;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.internal.runners.JUnit38ClassRunner;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 24/10/2558.
 */
@RunWith(JUnitParamsRunner.class)
public class FavoriteTest {
    FavoriteDao favoriteDao = Mockito.mock(FavoriteDao.class);
    FavoriteServiceImpl favoriteService = new FavoriteServiceImpl(favoriteDao);
    List<Favorite> favorite = new ArrayList<>();

    @Test
    public void testGetFavorite() {
//        Favorite a = new Favorite();
//        favorite.add(a);
        when(favoriteDao.getFavorite()).thenReturn(favorite);
        assertThat(favoriteService.getFavorite(), is(favorite));
        assertThat(favoriteService.getFavorite(null), is(nullValue()));
    }

    @Test
    public void testGetFavoriteWithID() {
        Favorite favorite1 = new Favorite();
        when(favoriteDao.getFavorite(1l)).thenReturn(favorite1);
        assertThat(favoriteService.getFavorite(1l), is(favorite1));
        assertThat(favoriteService.getFavorite(null), is(nullValue()));

    }

    @Test
    public void testAddFavorite() {
        Favorite favorite = new Favorite();
        when(favoriteDao.addFavorite(favorite)).thenReturn(favorite);
        assertThat(favoriteService.addFavorite(favorite), is(favorite));
        assertThat(favoriteService.addFavorite(null), is(nullValue()));
    }

    @Test
    public void testDeleteFavorite() {
        Favorite a = new Favorite();
        favorite.add(a);
        when(favoriteService.getFavorite(1L)).thenReturn(favorite.get(0));
        when(favoriteDao.deleteFavorite(favorite.get(0))).thenReturn(favorite.get(0));
        assertThat(favoriteService.deleteFavorite(1L), is(favorite.get(0)));
        assertThat(favoriteService.deleteFavorite(null), is(nullValue()));
    }

    @Test
    public void testGetFavoriteWithuserID() {

        when(favoriteDao.getFavoriteByUserId(1l)).thenReturn(favorite);
        assertThat(favoriteService.getFavoriteByUserId(1l), is(favorite));
        assertThat(favoriteService.getFavoriteByUserId(null), is(empty()));
//        assertThat(favoriteService.getFavoriteByUserId(null), is(nullValue()));

    }



}
