//package camt.se331.shoppingcart.entity;
//
//import camt.se331.shoppingcart.dao.FavoriteDao;
//import camt.se331.shoppingcart.dao.FavoriteDaoImpl;
//import camt.se331.shoppingcart.dao.MessageDao;
//import camt.se331.shoppingcart.dao.MessageDaoImlp;
//import camt.se331.shoppingcart.service.FavoriteServiceImpl;
//import camt.se331.shoppingcart.service.MessageServiceImlp;
//import junitparams.JUnitParamsRunner;
//import org.hamcrest.Matcher;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.*;
//import static junitparams.JUnitParamsRunner.$;
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.Mockito.*;
//
//import camt.se331.shoppingcart.entity.Message;
//import org.mockito.Mockito;
//
//@RunWith(JUnitParamsRunner.class)
//public class unittest {
//    MessageDao messageDao = Mockito.mock(MessageDao.class);
//    MessageServiceImlp messageDaoService = new MessageServiceImlp(messageDao);
//    List<Message> message = new ArrayList<>();
//
//    FavoriteDao favoriteDao = Mockito.mock(FavoriteDao.class);
//    FavoriteServiceImpl favoriteService = new FavoriteServiceImpl(favoriteDao);
//    List<Favorite> favorite = new ArrayList<>();
//
//    @Before
//    public void setUp() {
//        Message message1 = new Message();
//        message1.setId(1L);
//        message1.setMessage("goodmorning");
//        message1.setNameofuser("Mr.jame");
//        message1.setApartmentid(1L);
//        message1.setUserid(1l);
//        message1.setRoomchatid("1");
//        //  Message message2 = new Message();
//        message.add(message1);
//        //    message.add(message2);
//        when(messageDao.getMessage()).thenReturn(message);
//        when(messageDao.getMessage(1L)).thenReturn(message.get(0));
//        when(messageDao.getMessage(null)).thenReturn(null);
//
//        Favorite favorite1 = new Favorite();
//        favorite.add(favorite1);
//
//    }
//
//    @Test
//    public void testGetMessage() {
//        assertThat(messageDaoService.getMessage(), is(message));
//    }
//
//    @Test
//    public void testGetMessageWithID() {
//        assertThat(messageDaoService.getMessage(1L).getId(), is(1L));
//        assertThat(messageDaoService.getMessage(1L).getMessage(), is("goodmorning"));
//        assertThat(messageDaoService.getMessage(1L).getNameofuser(), is("Mr.jame"));
//        assertThat(messageDaoService.getMessage(1L).getApartmentid(), is(1L));
//        assertThat(messageDaoService.getMessage(1L).getUserid(), is(1L));
//        assertThat(messageDaoService.getMessage(1L).getRoomchatid(), is("1"));
//        assertThat(messageDaoService.getMessage(null), is(nullValue()));
//    }
//
//    @Test
//    public void testAddMessage() {
//        Message message3 = new Message();
//        when(messageDao.addMessage(message3)).thenReturn(message3);
//        assertThat(messageDaoService.addMessage(message3), is(message3));
//    }
//
//    @Test
//    public void testDeleteMessage() {
//        when(messageDaoService.getMessage(1L)).thenReturn(message.get(0));
//        when(messageDao.deleteMessage(message.get(0))).thenReturn(message.get(0));
//        assertThat(messageDaoService.deleteMessage(1L), is(message.get(0)));
//    }
//
//    @Test
//    public void testGetMessageByRoomchatid() {
//        when(messageDao.getMessageByRoomchatid("1")).thenReturn(message);
//        assertThat(messageDaoService.getMessageByRoomchatid("1"), is(message));
//
//    }
//
//    @Test
//    public void testGetMessageByApartmentid() {
//        when(messageDao.getMessageByApartmentId(1l)).thenReturn(message);
//        assertThat(messageDaoService.getMessageByApartmentId(1l),  is(message));
//    }
//
//    @Test
//    public void testGetMessageBy() {
//        when(messageDao.getMessageByApartmentId(1l)).thenReturn(message);
//        assertThat(messageDaoService.getMessageByApartmentId(1l),  is(message));
//    }
//
//    @Test
//    public void testGetfavorite(){
//        when(favoriteDao.getFavorite()).thenReturn(favorite);
//        assertThat(favoriteService.getFavorite(), is(favorite));
//
//    }
//
//
//
//
//}