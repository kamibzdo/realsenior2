package camt.se331.shoppingcart.entity;

import camt.se331.shoppingcart.dao.MessageDao;
import camt.se331.shoppingcart.service.MessageServiceImlp;
import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

/**
 * Created by Administrator on 24/10/2558.
 */
@RunWith(JUnitParamsRunner.class)
public class MessageTest {

    MessageDao messageDao = Mockito.mock(MessageDao.class);
    MessageServiceImlp messageService = new MessageServiceImlp(messageDao);
    List<Message> message = new ArrayList<>();

    @Test
    public void testGetMessage() {
        assertThat(messageService.getMessage(), is(message));
    }

    @Test
    public void testGetMessageWithID() {
        Message message1 = new Message();
        when(messageDao.getMessage(1l)).thenReturn(message1);
        assertThat(messageService.getMessage(1l), is(message1));
        assertThat(messageService.getMessage(null), is(nullValue()));

    }

    @Test
    public void testAddMessage() {
        Message message3 = new Message();
        when(messageDao.addMessage(message3)).thenReturn(message3);
        assertThat(messageService.addMessage(message3), is(message3));
        assertThat(messageService.addMessage(null), is(nullValue()));
    }

    @Test
    public void testDeleteMessage() {
        Message a = new Message();
        message.add(a);
        when(messageService.getMessage(1L)).thenReturn(message.get(0));
        when(messageDao.deleteMessage(message.get(0))).thenReturn(message.get(0));
        assertThat(messageService.deleteMessage(1L), is(message.get(0)));
        assertThat(messageService.deleteMessage(null), is(nullValue()));
    }

    @Test
    public void testGetMessageByRoomchatid() {
        when(messageDao.getMessageByRoomchatid("1")).thenReturn(message);
        assertThat(messageService.getMessageByRoomchatid("1"), is(message));
        assertThat(messageService.getMessageByRoomchatid(null), is(empty()));
//        assertThat(messageService.getMessageByRoomchatid(null), is(nullValue()));
    }

    @Test
    public void testGetMessageByApartmentid() {
        when(messageDao.getMessageByApartmentId(1l)).thenReturn(message);
        assertThat(messageService.getMessageByApartmentId(1l), is(message));
        assertThat(messageService.getMessageByApartmentId(null), is(empty()));
//        assertThat(messageService.getMessageByApartmentId(null), is(nullValue()));

    }




}
