'use strict';

// Declare app level module which depends on views, and components
var labApp = angular.module('labApp', [
    'ngRoute',
    'productMainController',
    'languageControllers',
    'languageServices',
    'pascalprecht.translate',
    'shoppingCartControllers',
    'flow',
    'securityControllers',
    'registerControllers',
    'apartmentControllers',
    'favoriteControllers',
    'compareControllers',
    'chatControllers',
    'myapp',
    'shoppingCartServices'
])
labApp.config(['$routeProvider',
  function($routeProvider) {
  $routeProvider.
      when('/addProduct',{
          templateUrl: 'template/editProduct.html',
          controller: 'addProductController'
      }).
      when('/editProduct/:id',{
          templateUrl: 'template/editProduct.html',
          controller: 'editProductController'
      }).
      when('/listProduct',{
          templateUrl: 'template/productList.html',
          controller: 'listProductController'
      }).
      when('/profile',{
          templateUrl: 'template/profile.html',
          controller: 'ProfileController'
      }).
      when('/showProfile',{
          templateUrl: 'template/showProfile.html',
          controller: 'ProfileController'
      }).
      when('/shoppingCart/:id',{
          templateUrl: 'template/shoppingCart.html',
          controller: 'showShoppingCartController'
      }).
      when('/shoppingCart',{
          templateUrl: 'template/shoppingCart.html',
          controller: 'showShoppingCartController'
      }).
      when('/historyCart',{
          templateUrl: 'template/shoppingCartHistory.html',
          controller: 'shoppingCartHistoryController'
      }).
      when('/register',{
          templateUrl: 'template/register.html',
          controller: 'addUserController'
      }).
      when('/PostPage',{
          templateUrl: 'template/PostPage.html',
          controller: 'editApartmentController'
      }).
      when('/addApartment',{
          templateUrl: 'template/addApartment.html',
          controller: 'addApartmentController'
      }).
      when('/editApartment',{
          templateUrl: 'template/editApartment.html',
          controller: 'editApartmentController'
      }).
      when('/deleteApartment/:id',{
          templateUrl: 'template/editApartment.html',
          controller: 'editApartmentController'
      }).
      when('/fixApartment/:id',{
          templateUrl: 'template/fixApartment.html',
          controller: 'fixApartmentController'
      }).
      when('/showApartment/:id',{
          templateUrl: 'template/showApartment.html',
          controller: 'showApartmentController'
      }).
      when('/favorite',{
          templateUrl: 'template/favorite.html',
          controller: 'addFavoriteController'
      }).
      when('/deleteFavorite/:id',{
          templateUrl: 'template/favorite.html',
          controller: 'addFavoriteController'
      }).
      when('/compareApartment',{
          templateUrl: 'template/compareApartment.html',
          controller: 'compareController'
      }).
      when('/deleteCompareApartment/:id',{
          templateUrl: 'template/favorite.html',
          controller: 'compareController'
      }).
      when('/chat',{
          templateUrl: 'template/chat.html',
          controller: 'chatController'
      }).
      when('/chat/:id',{
          templateUrl: 'template/chat.html',
          controller: 'chatController'
      }).
      when('/test',{
          templateUrl: 'template/test.html',
          controller: 'EventSimpleCtrl'
      }).
      when('/manager',{
          templateUrl: 'template/manager.html',
          controller: 'editApartmentController'
      }).
       otherwise({redirectTo: '/PostPage'});
}]);

labApp.config(function($translateProvider){
    $translateProvider.useUrlLoader('/messageBundle');
    $translateProvider.useStorage('UrlLanguageStorage');
    $translateProvider.preferredLanguage('en');
    $translateProvider.fallbackLanguage('en');
})

labApp.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        target: '',
        permanentErrors: [ 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 4,
        singleFile: false
    };
    flowFactoryProvider.on('catchAll', function (event) {
        console.log('catchAll', arguments);
    });
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
}]);

labApp.config(['$locationProvider', '$httpProvider', function($locationProvider, $httpProvider){
    /* Register error provider that shows message on failed requests or redirects to login page on
     * unauthenticated requests */
    $httpProvider.interceptors.push(function($q,$rootScope,$location){
        return {
            'responseError': function(rejection){
                var status = rejection.status;
                var config = rejection.config;
                var method = config.method;
                var url = config.url;

                if (status == 401){
                    $location.path("/PostPage");
                }else{
                    $rootScope.error = method + " on " + url + " failed with status " + status;
                }
                return $q.reject(rejection);
            }
        }
    });

    /* Registers auth token interceptor, auth token is either passed by header or by query parameter
     * as soon as there is an authenticated user */
    var exampleAppConfig = {
        /* When set to false a query parameter is used to pass on the auth token.
         * This might be desirable if headers don't work correctly in some
         * environments and is still secure when using https. */
        useAuthTokenHeader: true
    };

    $httpProvider.interceptors.push(function ($q,$rootScope,$location){
        return {
            'request' : function(config){
                if (angular.isDefined($rootScope.authToken)){
                    var authToken = $rootScope.authToken;
                    if (exampleAppConfig.useAuthTokenHeader){
                        config.headers['X-Auth-Token'] = authToken;
                    }else{
                        config.url = config.url + "?token=" + authToken;
                    }
                }
                return config || $q.when(config);
            }

        }
    })

}]).run(function($rootScope,$location,$cookieStore,UserService,cartManagement){
    $rootScope.$on('$viewContentLoaded',function(){
        delete $rootScope.error;
    });

    $rootScope.hasRole = function(role) {
        if ($rootScope.user == undefined){
            return false;
        }

        if ($rootScope.user.roles[role] == undefined){
            return false;
        }

        return $rootScope.user.roles[role];
    }

    $rootScope.logout = function(){
        if($rootScope.hasRole('user')) {
            cartManagement.emptyCart(function () {
                console.log("log out success");
            });
        }
        delete $rootScope.user;
        $location.path("/PostPage");
        $cookieStore.remove('authToken');

    }

    /* Try getting valid user from cookie or go to login page */
    var originalPath = $location.path();
    $location.path("/PostPage");
    var authToken = $cookieStore.get('authToken');
    if (authToken != undefined){
        $rootScope.authToken = authToken;
        UserService.get(function(user){
            $rootScope.user = user;
            $location.path(originalPath);
        })
    }
    $rootScope.initialized = true;
});
//test
labApp.directive('nxEqual', function() {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, model) {
            if (!attrs.nxEqual) {
                console.error('nxEqual expects a model as an argument!');
                return;
            }
            scope.$watch(attrs.nxEqual, function (value) {
                model.$setValidity('nxEqual', value === model.$viewValue);
            });
            model.$parsers.push(function (value) {
                var isValid = value === scope.$eval(attrs.nxEqual);
                model.$setValidity('nxEqual', isValid);
                return isValid ? value : undefined;
            });
        }
    };
});

function Ctrl($scope) {}

labApp.filter('startFrom', function () {
    return function (input, start) {
        start = +start;
        return input.slice(start);
    }
});

labApp.service('loadGoogleMapAPI', ['$window', '$q',
    function ( $window, $q ) {

        var deferred = $q.defer();

        // Load Google map API script
        function loadScript() {
            // Use global document since Angular's $document is weak
            var script = document.createElement('script');
            script.src = '//maps.googleapis.com/maps/api/js?sensor=false&language=en&callback=initMap';

            document.body.appendChild(script);
        }

        // Script loaded callback, send resolve
        $window.initMap = function () {
            deferred.resolve();
        }

        loadScript();

        return deferred.promise;
    }]);

labApp.directive('googleMap', ['$rootScope', 'loadGoogleMapAPI',
    function( $rootScope, loadGoogleMapAPI ) {

        return {
            restrict: 'C', // restrict by class name
            scope: {
                mapId: '@id', // map ID
                lat: '@',     // latitude
                long: '@'     // longitude
            },
            link: function( $scope, elem, attrs ) {

                // Check if latitude and longitude are specified
                if ( angular.isDefined($scope.lat) && angular.isDefined($scope.long) ) {

                    // Initialize the map
                    $scope.initialize = function() {
                        $scope.location = new google.maps.LatLng($scope.lat, $scope.long);

                        $scope.mapOptions = {
                            zoom: 12,
                            center: $scope.location
                        };

                        $scope.map = new google.maps.Map(document.getElementById($scope.mapId), $scope.mapOptions);

                        new google.maps.Marker({
                            position: $scope.location,
                            map: $scope.map,
                        });
                    }

                    // Loads google map script
                    loadGoogleMapAPI.then(function () {
                        // Promised resolved
                        $scope.initialize();
                    }, function () {
                        // Promise rejected
                    });
                }
            }
        };
    }]);