'use strict';

var chatController = angular.module('chatControllers', ['chatServices']);

chatController.controller('chatController', ['$scope', '$http', '$location', '$rootScope', 'compareService', '$routeParams', 'queryUser', 'apartmentService', 'chatService', '$route', 'getMessageFromRoomchatid', 'messageService', 'NotificationService','getChatFromRoomchatid',
    function ($scope, $http, $location, $rootScope, compareService, $routeParams, queryUser, apartmentService, chatService, $route, getMessageFromRoomchatid, messageService, NotificationService,getChatFromRoomchatid) {


        $scope.chat = {};
        $scope.notification = {};
        $rootScope.chat = {};
        if($rootScope.notification==undefined) {
            $rootScope.notification = {};
        }
        var id = $routeParams.id;
        $http.get("/Chat/" + id).success(function (data) {
            //$scope.apartment = data;

            getMessageFromRoomchatid.querychat({id: id}, function (data) {
                $scope.chatroomids = data;
                $scope.fromuser = $rootScope.user.id;
            })

            //getChatFromRoomchatid.querychat({id: id}, function (data) {
            //    $scope.chats = data
            //})


        });

        $scope.addChat = function (user) {
            $rootScope.notification = {};

            $scope.chat.fromid = $rootScope.user.id;
            $rootScope.chat.fromid = $rootScope.user.id;
            $scope.chat.toid = user.id;
            $scope.nameheadchat = user.name;
            $rootScope.notification.toid = user.id;
            $rootScope.notification.toname = user.name;
            console.log($rootScope.notification.toid);
            $scope.chat.roomchatid = createRoomchat();

            //$scope.notification.fromid = $rootScope.user.id;
            //$scope.notification.toid = user.id;
            //$scope.notification.link = "/chat/"+createRoomchat();
            //
            //
            //NotificationService.add($scope.notification, function () {
            //
            //})

            chatService.add($scope.chat, function () {

                //$route.reload();


                $location.path("/chat/" + $scope.chat.roomchatid)


            })




            //getMessageFromRoomchatid.querychat({id: $scope.chat.roomchatid}, function (data) {
            //    $scope.chatroomids = data;
            //});


        }

        var createRoomchat = function () {

            if ($scope.chat.fromid < $scope.chat.toid) {
                $scope.chat.roomchatid = $scope.chat.fromid + '00' + $scope.chat.toid;

            }
            if ($scope.chat.fromid > $scope.chat.toid) {

                $scope.chat.roomchatid = $scope.chat.toid + '00' + $scope.chat.fromid;

            }
            if ($scope.chat.fromid == $scope.chat.toid) {

                $scope.chat.roomchatid = null;

            }

            return $scope.chat.roomchatid;

        }

        console.log($rootScope.notification.toid);

        $scope.addMessage = function (yy) {
            $scope.message = {};
            $scope.message.messageContent = {};
            $scope.message.user = {};
            $scope.message.nameofuser = {};
            //$scope.message.apartmentid = {};
            $scope.message.userid = {};
            $scope.message.roomchatid = {};
            $scope.message.user.id = $rootScope.user.id;
            $scope.message.nameofuser = $rootScope.user.name;
            //$scope.message.apartmentid = $scope.apartment.id;
            $scope.message.userid = $rootScope.user.id;
            $scope.message.roomchatid = id;
            $scope.message.messageContent = yy;
            $scope.message.date = $scope.date;

            $scope.notification.fromid = $rootScope.user.id;
            $scope.notification.fromname = $rootScope.user.name;
                console.log($rootScope.notification.toid);
                $scope.notification.toid = $rootScope.notification.toid;
            $scope.notification.toname = $rootScope.notification.toname;
            //$scope.notification.toname = $scope.chat.toid;
            //$scope.notification.nameApartment = $scope.apartment.apartmentName;
            $scope.notification.link = "#/chat/"+id;
            $scope.notification.inboxContent = yy;


            messageService.add($scope.message, function () {



                //$location.path("/chat/"+$scope.chat.roomchatid);

            })

            NotificationService.add($scope.notification, function () {
                $route.reload();
            });


        };



        queryUser.queryuser(function (data) {
            // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);

            $scope.users = data;
            for(var i=0;i<$scope.users.length;i++) {
                if($scope.users[i].id == $rootScope.user.id) {
                    $scope.users.splice(i, 1);
                }
            }
        });


        $scope.deleteMessage = function (message) {

            var answer = confirm("Do you want to delete the message?");
            if (answer) {
                messageService.delete({id: message}, function () {

                    $route.reload();

                })
            }


        }

        $scope.date = new Date();

        $scope.userid = $rootScope.user.id;



        //apartmentService.query(function (data) {
        //    // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
        //    $scope.apartments = data;
        //});


    }]);
