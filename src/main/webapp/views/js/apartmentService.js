'use strict'

var apartmentService = angular.module('apartmentServices', ['ngResource']);

apartmentService.factory('apartmentService', function ($resource) {
    return $resource('/Apartment/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        }
        ,
        getApartment: {
            method: 'GET'
        },
        delete: {
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });

})

apartmentService.factory('queryApartmentService', function ($resource) {
    return $resource('/getApartment/',
        {
            query: {method: 'GET', isArray: true}

        });
})

apartmentService.factory('queryApartmentServiceByUser', function ($resource) {
    return $resource('/getApartmentFromUser/:id', {id: '@_id'},
        {
            query2: {method: 'GET', isArray: true}

        });
})

apartmentService.factory('deletePhotoService', function ($resource) {
    return $resource('/Apartment/photo/:id', {id: '@_id'}, {
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });
})

apartmentService.factory('favoriteService', function ($resource) {
    return $resource('/Favorite/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });
})


apartmentService.factory('messageService', function ($resource) {
    return $resource('/Message/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        },
        delete: {
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });
})

apartmentService.factory('queryMessageServiceByApartment', function ($resource) {
    return $resource('/getMessageFromApartment/:id', {id: '@_id'},
        {
            query3: {method: 'GET', isArray: true}

        });
})


//apartmentService.service('totalCalService',function() {
//    this.getTotalNetPrice = function (products) {
//        var output = 0.0;
//
//        for (var index = 0; index < products.length;index++) {
//            var product = products[index];
//            output += parseFloat(product.netPrice);
//        }
//        return output;
//    }
//})
//
//apartmentService.factory('queryApartmentService',function($resource){
//    return $resource('/getApartment/?name=:name',
//        {query:{method:'GET',params:{name:''},isArray:true}
//
//        });
//})
//apartmentService.factory('deleteImageService',function($resource)
//{
//    return $resource('/Apartment/image/:id', {id: '@_id'}, {
//        update: {
//            method: 'PUT' // this method issues a PUT request
//        }
//    });
//})