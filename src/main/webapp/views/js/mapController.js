'use strict';
var app = angular.module('myapp', ['ngMap']);
app.controller('EventSimpleCtrl', ['$scope', '$timeout', function ($scope, $timeout) {

        $scope.map = {
            center: {latitude: 30, longitude: 30 },
            zoom: 8,
            bounds: {},
            control: {},
            events: {
                tilesloaded: function (map) {
                    $scope.$apply(function () {
                        google.maps.event.trigger(map, "resize");
                    });
                }
            }
        };
        $scope.options = {scrollwheel: true};



}]);
