'use strict'

var favoriteService = angular.module('favoriteServices', ['ngResource']);

favoriteService.factory('favoriteService', function ($resource) {
    return $resource('/Favorite/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        }
        ,
        getApartment: {
            method: 'GET'
        },
        delete: {
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });

})


favoriteService.factory('queryFavoriteService', function ($resource) {
    return $resource('/getFavoriteFromUser/:id', {id: '@_id'},
        {
            query:{method: 'GET', isArray: true}

        });
})