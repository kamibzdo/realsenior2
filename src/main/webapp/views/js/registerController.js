'use strict';

var userMainController = angular.module('registerControllers', ['userServices']);

userMainController.controller('addUserController', ['$scope', '$http', '$location', '$rootScope','userService','chatService','queryUser',
    function ($scope, $http, $location, $rootScope,userService,chatService,queryUser) {

        $scope.user = {};

        //queryUser.queryuser(function (data) {
        //    // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
        //
        //    $scope.users = data;
        //});

        $scope.addUser = function () {
            //userService.test({id:1},{},function(returnValue)delete
            //userService.update({id:$scope.user.id},$scope.user,function(returnValue){

         userService.add({},$scope.user,function(returnValue){
             $rootScope.addUserSuccess = true;
             $rootScope.editUserSuccess = false;
             $rootScope.deleteUserSuccess = false;
                $location.path("home");


            });

            //$scope.chat = {};
            //$scope.chat.temp = $scope.user.username;
            //chatService.add({},$scope.chat,function(returnValue){
            //
            //
            //
            //});


        };


    }]);
userMainController.controller('ProfileController', ['$scope', '$http', '$location', '$rootScope','userService',

    function ($scope, $http, $location, $rootScope,userService) {
        //$scope.user = $rootScope.user;
        $scope.isEdit=true;

        userService.get({id:$rootScope.user.id},function(returnValue){
            // success

            $scope.user = returnValue;
            $scope.user.role= $scope.user.roles[0];

        })


        //$scope.user = {};
        //$scope.user.roles = null;
        $scope.editUser = function () {


            userService.update({id:$scope.user.id},$scope.user,function(returnValue){
                $rootScope.addUserSuccess = false;
                $rootScope.editUserSuccess = true;
                $rootScope.deleteUserSuccess = false;
                $location.path("home");

            });
        };

        $scope.deleteUser = function () {

            //$http.post("/product", $scope.product).success(function () {
            userService.delete({id:$scope.user.id},function(returnValue){
                //location.reload();
                $rootScope.addUserSuccess = false;
                $rootScope.editUserSuccess = false;
                $rootScope.deleteUserSuccess = true;
                $location.path("home");

            });
        };

    }]);


