'use strict';

var favoriteMainController = angular.module('favoriteControllers', ['favoriteServices']);

favoriteMainController.controller('addFavoriteController', ['$scope', '$http', '$location', '$rootScope', 'favoriteService','queryFavoriteService',
    function ($scope, $http, $location, $rootScope, favoriteService,queryFavoriteService) {


        queryFavoriteService.query({id: $rootScope.user.id}, function (data) {
            // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
            $scope.favorites = data;
        });


        $scope.deleteFavorite = function(id){
            var answer = confirm("Do you want to delete the apartment?");
            if (answer) {
                favoriteService.delete({id: id}, function () {

                    $rootScope.user.favorite = null;
                    $rootScope.deleteSuccess = true;

                })
            }
        }

        $scope.setClickedRow = function (id) {
            //$scope.selectedRow = index;
            $location.path("/showApartment/"+(id)); //function that sets the value of selectedRow to current index

        }



    }]);