'use strict';

var apartmentMainController = angular.module('apartmentControllers', ['apartmentServices']);

apartmentMainController.controller('addApartmentController', ['$scope', '$http', '$location', '$rootScope', 'apartmentService',
    function ($scope, $http, $location, $rootScope, apartmentService) {

        $scope.apartment = {};
        $scope.typeOfApartments = [];

        $scope.addApartment = function (flowFiles) {

            $scope.apartment.facilities = createFaculityString($scope.typeOfApartments);
            //$scope.apartment.user =$rootScope.user;\
            $scope.apartment.user = {};
            $scope.apartment.user.id = $rootScope.user.id;
            $scope.apartment.knowuserid = $rootScope.user.id;
            apartmentService.add({}, $scope.apartment, function (data) {

                $location.path("home");

                var apartmentid = data.id;
                // set location
                flowFiles.opts.target = '/apartmentPhoto/add';
                flowFiles.opts.testChunks = false;
                flowFiles.opts.query = {apartmentid: apartmentid};
                flowFiles.upload();

                $rootScope.addSuccess = true;
                $location.path("home");

                $scope.$apply();


            });
        };

        var createFaculityString = function (array) {
            var output = "";
            if (array[0] == true) {
                output = output + 'Aircondition' + ',';
            }
            if (array[1] == true) {
                output = output + 'Fan' + ',';
            }
            if (array[2] == true) {
                output = output + 'Waterheater' + ',';
            }
            if (array[3] == true) {
                output = output + 'Bed' + ',';
            }
            if (array[4] == true) {
                output = output + 'Beddingset' + ',';
            }
            if (array[5] == true) {
                output = output + 'Desk' + ',';
            }
            if (array[6] == true) {
                output = output + 'Dressingtable' + ',';
            }

            if (array[7] == true) {
                output = output + 'Closet' + ',';
            }
            if (array[8] == true) {
                output = output + 'Securityguard ' + ',';
            }
            if (array[9] == true) {
                output = output + 'Fingerprintsecurity' + ',';
            }
            if (array[10] == true) {
                output = output + 'Keycard' + ',';
            }
            if (array[11] == true) {
                output = output + 'Microwave' + ',';
            }
            if (array[12] == true) {
                output = output + 'Sink' + ',';
            }
            if (array[13] == true) {
                output = output + 'CableTV' + ',';
            }
            if (array[14] == true) {
                output = output + 'Telephone' + ',';
            }
            if (array[15] == true) {
                output = output + 'Internet' + ',';
            }
            if (array[16] == true) {
                output = output + 'Petallowed' + ',';
            }
            if (array[17] == true) {
                output = output + 'Smokingallowed ' + ',';
            }
            if (array[18] == true) {
                output = output + 'Parking' + ',';
            }
            if (array[19] == true) {
                output = output + 'Elevator' + ',';
            }
            if (array[20] == true) {
                output = output + 'InternetCafe' + ',';
            }
            if (array[21] == true) {
                output = output + 'CCTV' + ',';
            }
            if (array[22] == true) {
                output = output + 'Swimmingpool' + ',';
            }
            return output;
        }
    }]);

userMainController.controller('editApartmentController', ['$scope', '$http', '$location', '$rootScope', 'apartmentService', 'queryApartmentService', 'queryApartmentServiceByUser', '$route', 'deletePhotoService', 'favoriteService', 'compareService', 'queryUser','NotificationService',

    function ($scope, $http, $location, $rootScope, apartmentService, queryApartmentService, queryApartmentServiceByUser, $route, deletePhotoService, favoriteService, compareService, queryUser,NotificationService) {

        $scope.name="";
        $scope.address="";
        $scope.street="";
        $scope.subDistrict="";
        $scope.district="";
        $scope.city="";
        $scope.facilities="";
        $scope.isEdit = true;
        $scope.typeOfApartments = [];
        $scope.apartment = {};



        apartmentService.query(function (data) {
            // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
            $scope.apartments = data;
        });

        if ($rootScope.user != null) {
            queryApartmentServiceByUser.query2({id: $rootScope.user.id}, function (data) {
                // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
                $scope.apartmentsofuser = data;
            });
        }

        queryUser.queryuser(function (data) {
            // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);

            $scope.users = data;
        });


        //$scope.selectedRow = null;  // initialize our variable to null
        //$scope.apartment.id = index;

        $scope.setClickedRow = function (index) {
            //$scope.selectedRow = index;
            $location.path("/showApartment/" + (index + 1)); //function that sets the value of selectedRow to current index

        }

        $scope.favorite = {};

        $scope.addFavorite = function (apartment) {
            $scope.favorite.user = {};
            $scope.favorite.tempApartmentid = {};
            $scope.favorite.user.id = $rootScope.user.id;
            $scope.favorite.tempApartmentid = apartment.id;
            $scope.favorite.tempapartmentName = apartment.apartmentName;
            $scope.favorite.tempaddressNo = apartment.addressNo;
            $scope.favorite.tempstreet = apartment.street;
            $scope.favorite.tempsubDistrict = apartment.subDistrict;
            $scope.favorite.tempdistrict = apartment.district;
            $scope.favorite.tempcity = apartment.city;
            $scope.favorite.temprentalFee = apartment.rentalFee;

            favoriteService.add($scope.favorite, function () {


                $location.path("/favorite");


            })
        }

        $scope.compare = {};

        $scope.addCompare = function (apartment) {

            $scope.compare.user = {};
            $scope.compare.tempApartmentid = {};
            $scope.compare.user.id = $rootScope.user.id;
            $scope.compare.tempApartmentid = apartment.id;

            compareService.add($scope.compare, function () {


                $location.path("/compareApartment");

            })
        }


        $scope.deleteApartment = function (id) {
            var answer = confirm("Do you want to delete the apartment?");
            if (answer) {
                apartmentService.delete({id: id}, function () {

                    $rootScope.user.apartment = null;
                    $rootScope.deleteSuccess = true;
                    //$route.reload();

                })
            }
        };
        $scope.data = {};
        $scope.data.price="";
        $scope.searchApartment = function (price) {

            $scope.facilities = createFaculityString($scope.typeOfApartments);
               queryApartmentService.query({
                name: $scope.name,
                address: $scope.address,
                street: $scope.street,
                subDistrict: $scope.subDistrict,
                district: $scope.district,
                city: $scope.city,
                price: price,
                facilities: $scope.facilities
            }, function (data) {
                   $scope.apartments = data;
                   $rootScope.FailSearch = false;
                   if (data.length == 0)   {

                   $rootScope.FailSearch = true;

               }

            });
        };

        $scope.deletePhoto = function (photoId) {
            var answer = confirm("Do you want to delete the picture?");
            if (answer) {
                $scope.apartment.photos = null;
                deletePhotoService.update({id: photoId}, $scope.apartment, function (data) {
                    console.log("success");
                });
            }
        }

        var createFaculityString = function (array) {
            var output = "";
            if (array[0] == true) {
                output = output + 'Aircondition' + ',';
            }
            if (array[1] == true) {
                output = output + 'Fan' + ',';
            }
            if (array[2] == true) {
                output = output + 'Waterheater' + ',';
            }
            if (array[3] == true) {
                output = output + 'Bed' + ',';
            }
            if (array[4] == true) {
                output = output + 'Beddingset' + ',';
            }
            if (array[5] == true) {
                output = output + 'Desk' + ',';
            }
            if (array[6] == true) {
                output = output + 'Dressingtable' + ',';
            }

            if (array[7] == true) {
                output = output + 'Closet' + ',';
            }
            if (array[8] == true) {
                output = output + 'Securityguard ' + ',';
            }
            if (array[9] == true) {
                output = output + 'Fingerprintsecurity' + ',';
            }
            if (array[10] == true) {
                output = output + 'Keycard' + ',';
            }
            if (array[11] == true) {
                output = output + 'Microwave' + ',';
            }
            if (array[12] == true) {
                output = output + 'Sink' + ',';
            }
            if (array[13] == true) {
                output = output + 'CableTV' + ',';
            }
            if (array[14] == true) {
                output = output + 'Telephone' + ',';
            }
            if (array[15] == true) {
                output = output + 'Internet' + ',';
            }
            if (array[16] == true) {
                output = output + 'Petallowed' + ',';
            }
            if (array[17] == true) {
                output = output + 'Smokingallowed ' + ',';
            }
            if (array[18] == true) {
                output = output + 'Parking' + ',';
            }
            if (array[19] == true) {
                output = output + 'Elevator' + ',';
            }
            if (array[20] == true) {
                output = output + 'InternetCafe' + ',';
            }
            if (array[21] == true) {
                output = output + 'CCTV' + ',';
            }
            if (array[22] == true) {
                output = output + 'Swimmingpool' + ',';
            }
            return output;
        };




    }]);

userMainController.controller('fixApartmentController', ['$scope', '$http', '$routeParams', '$location', '$rootScope', 'apartmentService', 'queryApartmentService', 'queryApartmentServiceByUser', 'deletePhotoService',

    function ($scope, $http, $routeParams, $location, $rootScope, apartmentService, queryApartmentService, queryApartmentServiceByUser, deletePhotoService) {
        //$scope.user = $rootScope.user;

        var id = $routeParams.id;
        $http.get("/Apartment/" + id).success(function (data) {
            $scope.apartment = data;
        });

        //$scope.apartment = {};
        $scope.typeOfApartments = [];

        //var pic = $scope.apartment.photos;
        $scope.editApartment = function (flowFiles) {
            $scope.apartment.photos = null;
            //$scope.apartment.photos = pic;
            $scope.apartment.facilities = createFaculityString($scope.typeOfApartments);
            //$scope.apartment.user =$rootScope.user;\
            $scope.apartment.user = {};
            $scope.apartment.user.id = $rootScope.user.id;
            apartmentService.update({id: $scope.apartment.id}, $scope.apartment, function (data) {

                var apartmentid = data.id;
                flowFiles.opts.target = '/apartmentPhoto/add';
                flowFiles.opts.testChunks = false;
                flowFiles.opts.query = {apartmentid: apartmentid};
                flowFiles.upload();
                $rootScope.editSuccess = true;
                $location.path("home");
                $scope.$apply();


            });
        }

        var createFaculityString = function (array) {
            var output = "";
            if (array[0] == true) {
                output = output + 'Aircondition' + ',';
            }
            if (array[1] == true) {
                output = output + 'Fan' + ',';
            }
            if (array[2] == true) {
                output = output + 'Waterheater' + ',';
            }
            if (array[3] == true) {
                output = output + 'Bed' + ',';
            }
            if (array[4] == true) {
                output = output + 'Beddingset' + ',';
            }
            if (array[5] == true) {
                output = output + 'Desk' + ',';
            }
            if (array[6] == true) {
                output = output + 'Dressingtable' + ',';
            }

            if (array[7] == true) {
                output = output + 'Closet' + ',';
            }
            if (array[8] == true) {
                output = output + 'Securityguard ' + ',';
            }
            if (array[9] == true) {
                output = output + 'Fingerprintsecurity' + ',';
            }
            if (array[10] == true) {
                output = output + 'Keycard' + ',';
            }
            if (array[11] == true) {
                output = output + 'Microwave' + ',';
            }
            if (array[12] == true) {
                output = output + 'Sink' + ',';
            }
            if (array[13] == true) {
                output = output + 'CableTV' + ',';
            }
            if (array[14] == true) {
                output = output + 'Telephone' + ',';
            }
            if (array[15] == true) {
                output = output + 'Internet' + ',';
            }
            if (array[16] == true) {
                output = output + 'Petallowed' + ',';
            }
            if (array[17] == true) {
                output = output + 'Smokingallowed ' + ',';
            }
            if (array[18] == true) {
                output = output + 'Parking' + ',';
            }
            if (array[19] == true) {
                output = output + 'Elevator' + ',';
            }
            if (array[20] == true) {
                output = output + 'InternetCafe' + ',';
            }
            if (array[21] == true) {
                output = output + 'CCTV' + ',';
            }
            if (array[22] == true) {
                output = output + 'Swimmingpool' + ',';
            }
            return output;
        };

        $scope.deletePhoto = function (photoId) {
            var answer = confirm("Do you want to delete the picture?");
            if (answer) {
                $scope.apartment.photos = null;
                deletePhotoService.update({id: photoId}, $scope.apartment, function (data) {
                    console.log("success");
                });
            }
        }


    }]);

userMainController.controller('showApartmentController', ['$scope', '$http', '$routeParams', '$location', '$rootScope', 'apartmentService', 'queryApartmentService', 'queryApartmentServiceByUser', 'messageService', 'queryMessageServiceByApartment', '$route','chatService','NotificationService','favoriteService','$timeout','compareService',

    function ($scope, $http, $routeParams, $location, $rootScope, apartmentService, queryApartmentService, queryApartmentServiceByUser, messageService, queryMessageServiceByApartment, $route,chatService,NotificationService,favoriteService,$timeout,compareService) {
        //$scope.user = $rootScope.user;

        $scope.chat = {};
        $scope.notification = {};
        $scope.date = new Date();

        var id = $routeParams.id;
        $http.get("/Apartment/" + id).success(function (data) {
            $scope.apartment = data;
            queryMessageServiceByApartment.query3({id: id}, function (data) {
                // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
                $scope.messages = data;
            });
            var facilities= data.facilities.split(",") ;
            for(var i=0;i<facilities.length;i++){
                switch(facilities[i]){
                    case "Aircondition":$scope.airCondition = true;break;
                    case "Fan":$scope.Fan = true;break;
                    case "Waterheater":$scope.Waterheater = true;break;
                    case "Bed":$scope.Bed = true;break;
                    case "Beddingset":$scope.Beddingset = true;break;
                    case "Desk":$scope.Desk = true;break;
                    case "Dressingtable":$scope.Dressingtable = true;break;
                    case "Closet":$scope.Closet = true;break;
                    case "Securityguard":$scope.Securityguard = true;break;
                    case "Fingerprintsecurity":$scope.Fingerprintsecurity = true;break;
                    case "Keycard":$scope.Keycard = true;break;
                    case "Microwave":$scope.Microwave = true;break;
                    case "Sink":$scope.Sink = true;break;
                    case "CableTV":$scope.CableTV = true;break;
                    case "Telephone":$scope.Telephone = true;break;
                    case "Internet":$scope.Internet = true;break;
                    case "Petallowed":$scope.Petallowed = true;break;
                    case "Smokingallowed":$scope.Smokingallowed = true;break;
                    case "Parking":$scope.Parking = true;break;
                    case "Elevator":$scope.Elevator = true;break;
                    case "InternetCafe":$scope.InternetCafe = true;break;
                    case "CCTV":$scope.CCTV = true;break;
                    case "Swimmingpool":$scope.Swimmingpool = true;break;  }
            }
        });

        $scope.addMessage = function (zz) {

            $scope.message = {};
            $scope.message.user = {};
            $scope.message.messageContent = {};
            $scope.notification.messageContent = {};
            $scope.message.nameofuser = {};
            $scope.message.apartmentid = {};
            $scope.message.userid = {};
            $scope.message.user.id = $rootScope.user.id;
            $scope.message.nameofuser = $rootScope.user.name;
            $scope.message.apartmentid = $scope.apartment.id;
            $scope.message.userid = $rootScope.user.id;
            $scope.message.messageContent = zz;
            $scope.message.date = $scope.date;

            $scope.notification.fromid = $rootScope.user.id;
            $scope.notification.fromname = $rootScope.user.name;
            $scope.notification.toid = $scope.apartment.knowuserid;
            $scope.notification.toname = $scope.apartment.contractPerson;
            $scope.notification.nameApartment = $scope.apartment.apartmentName;
            $scope.notification.link = "#/showApartment/"+$scope.apartment.id;
            $scope.notification.messageContent = zz;


            messageService.add($scope.message, function () {


                //$location.path("/compareApartment");

            })

            NotificationService.add($scope.notification, function () {
                $route.reload();
            });

        }
        //$scope.getMessage = function(id){
        //    for(var i = 0;i< $scope.messages.length;i++){
        //        if($scope.messages[i].id = id) {
        //            return $scope.messages[i];
        //        }
        //    }
        //    return null;
        //}

        $scope.addReply = function (message) {
            $scope.message = {};

            $scope.message.user = {};
            $scope.message.nameofuser = {};
            $scope.message.apartmentid = {};
            $scope.message.userid = {};
            $scope.message.replyid = {};
            $scope.message.message = {};
            $scope.message.user.id = $rootScope.user.id;
            $scope.message.nameofuser = $rootScope.user.name;
            $scope.message.apartmentid = $scope.apartment.id;
            $scope.message.userid = $rootScope.user.id;
            $scope.message.replyid = message.id;
            //var message = $scope.getMessage(id);
            $scope.message.replymessage = message.replymessage;
            $scope.message.date = $scope.date;


            $scope.notification.fromid = $rootScope.user.id;
            $scope.notification.fromname = $rootScope.user.name;
            $scope.notification.toid = message.userid;
            $scope.notification.toname = message.nameofuser;
            $scope.notification.nameApartment = $scope.apartment.apartmentName;
            $scope.notification.link = "#/showApartment/"+$scope.apartment.id;
            $scope.notification.messageContent = message.messageContent;


            messageService.add($scope.message, function () {

                $route.reload();
                //$location.path("/compareApartment");

            })

            NotificationService.add($scope.notification, function () {

            });

        }

        //$scope.addChat = function (message) {
        //
        //
        //    messageService.add(message, function () {
        //
        //        $route.reload();
        //        $location.path("/compareApartment");
        //
        //    })
        //
        //
        //}

        $scope.deleteMessage = function (message) {

            var answer = confirm("Do you want to delete the message?");
            if (answer) {
                messageService.delete({id: message}, function () {

                    $route.reload();

                })
            }


        };


        $scope.addChat = function (message) {


            $scope.chat.fromid = $rootScope.user.id;
            //$scope.chat.toid = $scope.apartment.knowuserid;
            $scope.chat.toid = message.userid;
            $scope.chat.roomchatid = createRoomchat();

            $scope.notification.fromid = $rootScope.user.id;
            $scope.notification.toid = $scope.apartment.knowuserid;
            $scope.notification.link = "#/chat/"+createRoomchat();


            NotificationService.add($scope.notification, function () {

            });

            chatService.add($scope.chat, function () {

                //$route.reload();
                $location.path("/chat/"+$scope.chat.roomchatid);

            });

            //getMessageFromRoomchatid.querychat({id: $scope.chat.roomchatid}, function (data) {
            //    $scope.chatroomids = data;
            //});



        };

        var createRoomchat = function () {

            if ($scope.chat.fromid < $scope.chat.toid) {
                $scope.chat.roomchatid = $scope.chat.fromid+'00'+$scope.chat.toid;

            }
            if($scope.chat.fromid > $scope.chat.toid){

                $scope.chat.roomchatid = $scope.chat.toid+'00'+$scope.chat.fromid;

            }
            if($scope.chat.fromid == $scope.chat.toid){

                $scope.chat.roomchatid = null;

            }

            return $scope.chat.roomchatid;

        }
        //var id = $routeParams.id;
        //$http.get("/Message/" + id).success(function (data) {
        //    $scope.message = data;
        //});


        //queryMessageServiceByApartment.query3({id:1}, function (data) {
        //    // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
        //    $scope.messages = data;
        //});

        $scope.favorite = {};

        $scope.addFavorite = function (apartment) {
            $scope.favorite.user = {};
            $scope.favorite.tempApartmentid = {};
            $scope.favorite.user.id = $rootScope.user.id;
            $scope.favorite.tempApartmentid = apartment.id;
            $scope.favorite.tempapartmentName = apartment.apartmentName;
            $scope.favorite.tempaddressNo = apartment.addressNo;
            $scope.favorite.tempstreet = apartment.street;
            $scope.favorite.tempsubDistrict = apartment.subDistrict;
            $scope.favorite.tempdistrict = apartment.district;
            $scope.favorite.tempcity = apartment.city;
            $scope.favorite.temprentalFee = apartment.rentalFee;

            favoriteService.add($scope.favorite, function () {


                $location.path("/favorite");


            })
        }

        $scope.compare = {};

        $scope.addCompare = function (apartment) {

            $scope.compare.user = {};
            $scope.compare.tempApartmentid = {};
            $scope.compare.user.id = $rootScope.user.id;
            $scope.compare.tempApartmentid = apartment.id;

            compareService.add($scope.compare, function () {


                $location.path("/compareApartment");

            })
        }




$scope.loadMap = function(){
    google.maps.event.trigger(map, 'resize');
    map.setZoom( map.getZoom() );
};


        var marker, map;
        $scope.$on('mapInitialized', function (evt, evtMap) {
            map = evtMap;
            marker = map.markers[0];
        });
        $scope.centerChanged = function (event) {
            $timeout(function () {
                map.panTo(marker.getPosition());
            }, 3000);
        }
        $scope.click = function (event) {
            map.setZoom(8);
            map.setCenter(marker.getPosition());
        }

    }]);


