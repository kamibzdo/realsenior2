
'use strict'

var userService = angular.module('userServices',['ngResource']);

userService.factory('userService',function($resource){
    return $resource('/user/user/:id', { id: '@_id' }, {
        add:{
            method: 'POST'
        },
        getUser:{
            method: 'GET'
        },
        getUser007:{
            method: 'GET'
        },
        delete:{
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }});

})