'use strict'

var chatService = angular.module('chatServices', ['ngResource']);

chatService.factory('chatService', function ($resource) {
    return $resource('/Chat/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        }
        ,
        getApartment: {
            method: 'GET'
        },
        delete: {
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });

})

chatService.factory('NotificationService', function ($resource) {
    return $resource('/Notification/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        }
        ,
        get: {
            method: 'GET', isArray: true
        },
        delete: {
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });

})

chatService.factory('queryUser', function ($resource) {
    return $resource('/user/user/:id', {id: '@_id'}, {
        queryuser: {
            method: 'GET', isArray: true
        }

    });

})

chatService.factory('getMessageFromRoomchatid', function ($resource) {
    return $resource('/getMessage/:id', {id: '@_id'},{
        querychat: {
            method: 'GET', isArray: true
        }

    });
})

chatService.factory('getChatFromRoomchatid', function ($resource) {
    return $resource('/getChatByChatroomid/:id', {id: '@_id'},{
        querychat: {
            method: 'GET', isArray: true
        }

    });
})

