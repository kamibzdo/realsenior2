'use strict'

var compareService = angular.module('compareServices', ['ngResource']);

compareService.factory('compareService', function ($resource) {
    return $resource('/Compare/:id', {id: '@_id'}, {
        add: {
            method: 'POST'
        }
        ,
        getCompare: {
            method: 'GET'
        },
        delete: {
            method: 'DELETE'
        },
        update: {
            method: 'PUT' // this method issues a PUT request
        }
    });

})

favoriteService.factory('getCompareFromUser', function ($resource) {
    return $resource('/getCompareFromUser/:id', {id: '@_id'},
        {
            query:{method: 'GET', isArray: true}

        });
})