'use strict';

var compareController = angular.module('compareControllers', ['compareServices']);

compareController.controller('compareController', ['$scope', '$http', '$location', '$rootScope', 'compareService', '$routeParams', 'apartmentService','getCompareFromUser',
    function ($scope, $http, $location, $rootScope, compareService, $routeParams, apartmentService,getCompareFromUser) {
$scope.Microwave = Array();
$scope.Keycard = Array();
$scope.Swimmingpool = Array();
$scope.Fan = Array();
 $scope.airCondition = Array();
        apartmentService.query(function (data) {
            // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);

            $scope.apartments = data;

            for(var i=0;i<data.length;i++) {
                var icon = data[i].facilities.split(",");
                for (var j = 0; j < icon.length; j++) {
                    switch (icon[j]) {
                        case "Aircondition":
                            $scope.airCondition[i] = true;
                            break;
                        case "Fan":
                            $scope.Fan[i] = true;
                            break;
                        case "Waterheater":
                            $scope.Waterheater[i] = true;
                            break;
                        case "Bed":
                            $scope.Bed[i] = true;
                            break;
                        case "Beddingset":
                            $scope.Beddingset[i] = true;
                            break;
                        case "Desk":
                            $scope.Desk[i] = true;
                            break;
                        case "Dressingtable":
                            $scope.Dressingtable[i] = true;
                            break;
                        case "Closet":
                            $scope.Closet[i] = true;
                            break;
                        case "Securityguard":
                            $scope.Securityguard[i] = true;
                            break;
                        case "Fingerprintsecurity":
                            $scope.Fingerprintsecurity[i] = true;
                            break;
                        case "Keycard":
                            $scope.Keycard[i] = true;
                            break;
                        case "Microwave":
                            $scope.Microwave[i] = true;
                            break;
                        case "Sink":
                            $scope.Sink[i] = true;
                            break;
                        case "CableTV":
                            $scope.CableTV[i] = true;
                            break;
                        case "Telephone":
                            $scope.Telephone[i] = true;
                            break;
                        case "Internet":
                            $scope.Internet[i] = true;
                            break;
                        case "Petallowed":
                            $scope.Petallowed[i] = true;
                            break;
                        case "Smokingallowed":
                            $scope.Smokingallowed[i] = true;
                            break;
                        case "Parking":
                            $scope.Parking[i] = true;
                            break;
                        case "Elevator":
                            $scope.Elevator[i] = true;
                            break;
                        case "InternetCafe":
                            $scope.InternetCafe[i] = true;
                            break;
                        case "CCTV":
                            $scope.CCTV[i] = true;
                            break;
                        case "Swimmingpool":
                            $scope.Swimmingpool[i] = true;
                            break;
                    }
                }
            }
        });


        getCompareFromUser.query({id: $rootScope.user.id}, function (data) {
            // $scope.totalNetPrice= totalCalService.getTotalNetPrice(data);
            $scope.compares = data;


        });



        $scope.deleteCompareApartment = function (id) {
            var answer = confirm("Do you want to delete the apartment?");
            if (answer) {
                compareService.delete({id: id}, function () {

                    $rootScope.user.compares = null;
                    $rootScope.deleteSuccess = true;
                    $location.path("/compareApartment");

                })
            }


        }


    }]);
