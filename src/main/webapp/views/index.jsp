<!DOCTYPE html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en" ng-app="labApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My AngularJS App</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%--<link rel="stylesheet" href="bower_components/html5-boilerplate/css/normalize.css">--%>
    <%--<link rel="stylesheet" href="bower_components/html5-boilerplate/css/main.css">--%>
    <%--<link rel="stylesheet" href="app.css">--%>
    <%--<link rel="stylesheet" href="animations.css">--%>
    <%--<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">--%>
    <%--<link href="http://r1.renthub.in.th/assets/application-c768fc6808d0e97d379ccc60bb7b7c9c.css" media="all"--%>
    <%--rel="stylesheet" type="text/css"/>--%>
    <!-- Bootstrap core CSS -->
    <link href="templateResource/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <%--<link href='http://fonts.googleapis.com/css?family=Raleway:400,600,800' rel='stylesheet' type='text/css'>--%>
    <link href="templateResource/css/font-awesome.min.css" rel="stylesheet">
    <link href="templateResource/css/style.css" rel="stylesheet">
    <link href="templateResource/css/responsive.css" rel="stylesheet">


    <script src="bower_components/html5-boilerplate/js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="bower_components/angular/angular.js"></script>

    <script src="bower_components/angular-animate/angular-animate.js"></script>

    <script src="bower_components/angular-route/angular-route.js"></script>
    <script src="bower_components/angular-resource/angular-resource.js"></script>
    <script src="bower_components/angular-cookies/angular-cookies.js"></script>
    <%--<script src="bower_components/jquery/dist/jquery.min.js"></script>--%>
    <%--<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>--%>
    <script src="bower_components/ng-flow/dist/ng-flow-standalone.js"></script>

    <script src="app.js"></script>
    <script src="js/productController.js"></script>
    <script src="js/productService.js"></script>

    <!-- Bootstrap core JavaScript
   ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="templateResource/js/jquery.js"></script>
    <script src="templateResource/js/bootstrap.js"></script>
    <%--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>--%>
    <script src="templateResource/js/gmap3.min.js"></script>
    <script src="templateResource/js/jquery.easing.js"></script>
    <script src="templateResource/js/jquery.jcarousel.min.js"></script>
    <script src="templateResource/js/imagesloaded.pkgd.min.js"></script>
    <script src="templateResource/js/masonry.pkgd.min.js"></script>
    <script src="templateResource/js/jquery.nicescroll.min.js"></script>
    <script src="templateResource/js/script.js"></script>

    <!-- add i18n script -->
    <script src="bower_components/angular-translate/angular-translate.js"></script>
    <script src="bower_components/angular-translate-loader-url/angular-translate-loader-url.js"></script>
    <script src="js/languageServices.js"></script>
    <script src="js/languageControllers.js"></script>
    <script src="js/shoppingCartController.js"></script>
    <script src="js/shoppingCartService.js"></script>
    <script src="js/securityController.js"></script>
    <script src="js/registerController.js"></script>
    <script src="js/registerService.js"></script>
    <script src="js/apartmentController.js"></script>
    <script src="js/apartmentService.js"></script>
    <script src="js/favoriteController.js"></script>
    <script src="js/favoriteService.js"></script>
    <script src="js/compareController.js"></script>
    <script src="js/compareService.js"></script>
    <script src="js/chatController.js"></script>
    <script src="js/chatService.js"></script>
    <script src="js/mapController.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather,visualization,panoramio"></script>
    <script src="http://code.angularjs.org/1.2.25/angular.js"></script>
    <script src="https://rawgit.com/allenhwkim/angularjs-google-maps/master/build/scripts/ng-map.js"></script>
    <%--<script src="script.js"></script>--%>
    <%--<link rel="stylesheet" href="style.css"/>--%>

</head>
<body id="top">
<div class="alert alert-success alert-dismissible" role="alert" ng-show="addUserSuccess">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Register is successful
</div>
<div class="alert alert-success alert-dismissible" role="alert" ng-show="editUserSuccess">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Your information is updated!
</div>
<div class="alert alert-success alert-dismissible" role="alert" ng-show="deleteUserSuccess">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> Success delete the user
</div>
<!-- begin:topbar -->
<div class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="topbar-nav topbar-right hidden-xs">
                    <li ng-show="hasRole('admin')||hasRole('user')||hasRole('Administrator')">
                        <div>
                            <h4> Hello !!! {{user.username}}</h4>
                            <%--email:{{user.email}}--%>
                            <%--password:{{user.password}}--%>
                            <%--username:{{user.username}}--%>
                            <%--id:{{user.id}}--%>
                            <%--role: {{user.role.roleName}}<br>--%>
                            <%--roles: {{user.roles}}--%>
                            <%--apartmentname: {{user.apartment[0].apartmentName}}--%>

                        </div>
                        <%--<div ng-repeat="Notification in Notifications">--%>
                        <%--{{$index}}--%>
                        <%----%>
                        <%----%>
                        <%--</div>--%>
                    </li>
                    <li class="dropdown" ng-show="hasRole('admin')||hasRole('user')||hasRole('Administrator')">

                        <a href="" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-comment"></i>
                            <span ng-repeat="Notification in Notifications" ng-if="user.id == Notification.toid&&Notification.inboxContent !=null"><b class="noti_bubble"><h5>!</h5></b></span></a>
                        <ul class="dropdown-menu">

                            <ul class="list-unstyled" ng-repeat="Notification in Notifications">
                                 <span ng-if="user.id == Notification.toid&&Notification.inboxContent !=null">
                            <li><a class="donutNotification" href="{{Notification.link}}"><img
                                    src="templateResource/img/usericon.png" style="width : 50px;height: 50px "
                                    class="avatar"
                                    alt="danish personal blog and magazine template"> Message from
                                {{Notification.fromname}}</a>
                                <hr style='background-color:#d9edf7; border-width:0; color:#000000; height:1px; line-height:0;  text-align: left; width:100%; margin-top:0px; margin-bottom:0px;'/>
                            </li>

                             </span>

                            </ul>

                        </ul>

                    </li>
                    <li class="dropdown" ng-show="hasRole('admin')||hasRole('user')||hasRole('Administrator')">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-globe"></i>
                            <span  ng-repeat="Notification in Notifications" ng-if="user.id == Notification.toid&&Notification.messageContent !=null" ><b class="noti_bubble">!</b></span></a>
                        <ul class="dropdown-menu">
                            <ul class="list-unstyled" ng-repeat="Notification in Notifications">
                                 <span ng-if="user.id == Notification.toid&&Notification.messageContent !=null">
                            <li><a class="donutNotification" href="{{Notification.link}}"><img
                                    src="templateResource/img/usericon.png" style="width : 50px;height: 50px "
                                    class="avatar"
                                    alt="danish personal blog and magazine template"> Comment from
                                {{Notification.fromname}}</a>

                            </li>

                             </span>

                            </ul>
                        </ul>
                    </li>
                    <li style="line-height:0.5;" ng-show="hasRole('admin')||hasRole('user')||hasRole('Administrator')">
                        <a href="#" class="signup" data-toggle="modal"
                           style="width:80px;height: 30px;margin-top: 5px;margin-bottom:  5px;padding-bottom: 10px;"
                           ng-click="logout()" data-target="#modal-signup">Sign out</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- begin:navbar -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-top">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html"><img src="templateResource/img/Drawing (2).png" alt="fdhdfh"><!-- <span>MIKHA</span> -->
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-top">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Home</a></li>
                <li class="dropdown" ng-show="hasRole('admin')">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown">Property management<b
                            class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#editApartment">My property</a></li>
                        <li><a href="#addApartment">Submit property</a></li>
                    </ul>
                </li>
                <li ng-show="hasRole('admin')||hasRole('user')||hasRole('Administrator')"><a href="#chat">Message</a>
                </li>
                <li ng-show="hasRole('user')"><a href="#favorite">My Favourite</a></li>
                <li ng-show="hasRole('user')"><a href="#compareApartment">Compare</a></li>
                <li ng-show="hasRole('admin')||hasRole('user')||hasRole('Administrator')"><a href="#showProfile">My
                    Account</a></li>
                <li ng-show="hasRole('Administrator')"><a href="#manager">All Apartment</a></li>
                <li><a href="#contact">Contact</a></li>
                <li ng-hide="hasRole('admin')||hasRole('user')||hasRole('Administrator')"><a href="#modal-signin"
                                                                                             class="signin"
                                                                                             data-toggle="modal"
                                                                                             data-target="#modal-signin">Sign
                    in</a></li>
                <li ng-hide="hasRole('admin')||hasRole('user')||hasRole('Administrator')"><a href="#modal-signup"
                                                                                             class="signup"
                                                                                             data-toggle="modal"
                                                                                             data-target="#modal-signup">Sign
                    up</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- begin:modal-signup -->
<div class="modal fade" id="modal-signup" tabindex="-1" role="dialog" aria-labelledby="modal-signup" aria-hidden="true"
     ng-controller="addUserController">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sign up</h4>
            </div>
            <div class="modal-body">
                <form name="editform" role="form" novalidate flow-init>
                    <div class="form-group">
                        <!-- Sidebar content-->
                        <input ng-model="user.name" class="form-control" id="name" name="name"
                               placeholder="Name" required/>

                        <div class="alert alert-danger" role="alert" ng-show="editform.name.$error.required">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Name is Required!
                        </div>

                    </div>
                    <div class="form-group">
                        <!-- Sidebar content-->
                        <input ng-model="user.username" class="form-control" id="username" name="username"
                               placeholder="Username" required/>

                        <div class="alert alert-danger" role="alert" ng-show="editform.username.$error.required">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Username is Required!
                        </div>
                    </div>
                    <div ng-controller='Ctrl'>
                        <div class="form-group">
                            <input ng-model="user.password" class="form-control" id="password"
                                   placeholder="Password" name="password" type="password" ng-minlength="8" required/>

                            <div class="alert alert-danger" role="alert" ng-show="editform.password.$error.required">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                Password is Required!
                            </div>

                            <div class="alert alert-danger" role="alert" ng-show="editform.password.$error.minlength">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                password must more than 8 characters
                            </div>
                        </div>
                        <div class="form-group">
                            <input ng-model="user.repassword" class="form-control" id="repassword"
                                   placeholder="rePassword" name="repassword" type="password" required
                                   nx-equal="user.password"/>

                            <div class="alert alert-danger" role="alert" ng-show="editform.repassword.$error.required">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                RePassword is Required!
                            </div>
                            <div class="alert alert-danger" ng-show="editform.repassword.$error.nxEqual">Password and
                                Re-password Must be equal!
                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <!-- Sidebar content-->
                        <input ng-model="user.email" class="form-control" id="email" name="email"
                               placeholder="Email" type="email" required/>

                        <div class="alert alert-danger" role="alert" ng-show="editform.email.$error.required">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Email is Required!
                        </div>
                        <div class="alert alert-danger" role="alert" ng-show="editform.email.$error.email">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Invalid format of e-mail
                        </div>
                    </div>

                    <label>
                        <input type="radio" ng-model="user.role.roleName" value="admin" name="admin">
                        Apartment Owner
                    </label><br/>

                    <label>
                        <input type="radio" ng-model="user.role.roleName" value="user" name="user">
                        Apartment Seeker
                    </label><br/>
                </form>
            </div>
            <div class="modal-footer">
                <p>Already have account ? <a href="#modal-signin" data-toggle="modal" data-target="#modal-signin">Sign
                    in here.</a></p>
                <input type="submit" class="btn btn-success btn-block btn-lg" ng-click="addUser()" value="Sign up">
            </div>
        </div>
    </div>
</div>
<!-- end:modal-signup -->

<!-- begin:modal-signin -->
<div class="modal fade" id="modal-signin" tabindex="-1" role="dialog" aria-labelledby="modal-signin" aria-hidden="true"
     ng-controller="loginController">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sign in</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="username">Email address</label>
                        <input type="username" ng-model="username" class="form-control input-lg"
                               placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" ng-model="password" class="form-control input-lg" placeholder="Password">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="forget" ng-model="rememberMe"> Keep me logged in
                        </label>
                    </div>
                    <div style="color:red">
                        {{error}}
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p>Don't have account ? <a href="#modal-signup" data-toggle="modal" data-target="#modal-signup">Sign up
                    here.</a></p>
                <input type="submit" ng-click="login()"
                       class="btn btn-success btn-block btn-lg"   <%--data-dismiss="modal"--%> value="Sign in">
            </div>
        </div>
    </div>
</div>

<!-- end:modal-signin -->
<!-- end:navbar -->
<ng-view>Loading...</ng-view>
<!-- In production use:
   <script src="//ajax.googleapis.com/ajax/libs/angularjs/x.x.x/angular.min.js"></script>
   -->
</body>
</html>
