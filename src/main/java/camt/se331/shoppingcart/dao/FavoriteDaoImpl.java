package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Favorite;
import camt.se331.shoppingcart.repository.FavoriteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 1/10/2558.
 */
@Repository
public class FavoriteDaoImpl implements FavoriteDao {

    @Autowired
    FavoriteRepository favoriteRepository;

    @Override
    public List<Favorite> getFavorite() {
        return favoriteRepository.findAll();
    }

    @Override
    public List<Favorite> getFavoriteByUserId(Long id) {
        return favoriteRepository.findByUserId(id);
    }

    @Override
    public Favorite getFavorite(Long id) {
        return favoriteRepository.findOne(id);
    }

    @Override
    public Favorite addFavorite(Favorite favorite) {
        return favoriteRepository.save(favorite);
    }

    @Override
    public Favorite deleteFavorite(Favorite favorite) {
        favoriteRepository.delete(favorite);
        favorite.setId(null);
        return favorite;

    }
}
