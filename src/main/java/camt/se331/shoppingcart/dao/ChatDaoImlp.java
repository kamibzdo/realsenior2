package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Chat;
import camt.se331.shoppingcart.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 11/10/2558.
 */
@Repository
public class ChatDaoImlp implements ChatDao {

    @Autowired
    ChatRepository chatRepository;


    @Override
    public List<Chat> getChat() {
        return chatRepository.findAll();
    }

    @Override
    public List<Chat> getChatByUserId(Long id) {
        return chatRepository.findByUserId(id);
    }

    @Override
    public List<Chat> getChatByChatroomid(Long id) {
        return chatRepository.findByRoomchatid(id);
    }

    @Override
    public Chat getChat(Long id) {
        return chatRepository.findOne(id);
    }

    @Override
    public Chat addChat(Chat chat) {
        return chatRepository.save(chat);
    }

    @Override
    public Chat deleteChat(Chat chat) {
        chatRepository.delete(chat);
        chat.setId(null);
        return chat;
    }
}
