package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Apartment;

import java.util.List;

/**
 * Created by Administrator on 1/8/2558.
 */
public interface ApartmentDao {
    List<Apartment> getApartment();
    List<Apartment> getApartmentBySearch(String name,String address,String street,String subDistrict,String district,String city,String price,String facilities);
    //    List<Apartment> getUserByName(String name);
    Apartment getApartment(Long id);
    Apartment addApartment(Apartment apartment);
    Apartment deleteApartment(Apartment apartment);
    Apartment updateApartment(Apartment apartment);
    List<Apartment> getApartmentsByUserId(Long id);
}
