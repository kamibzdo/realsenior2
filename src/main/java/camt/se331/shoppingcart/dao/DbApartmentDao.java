package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Apartment;
import camt.se331.shoppingcart.repository.ApartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Administrator on 1/8/2558.
 */
@Repository
public class DbApartmentDao implements ApartmentDao{
    @Autowired
    ApartmentRepository apartmentRepository;
    @Override
    public List<Apartment> getApartment() {
        return apartmentRepository.findAll();
    }

    @Override
    public List<Apartment> getApartmentBySearch(String name,String address,String street,String subDistrict,String district,String city,String price,String facilities) {
        String[] fa = facilities.split(",");
        String[] price2 = price.split("-");
        Double firstPrice= Double.valueOf(price2[0]);
        Double secondPrice= Double.valueOf(price2[1]);
        if(!fa[0].equals("")) {
            List<Apartment> b = new ArrayList<>();
            List<Apartment> a = new ArrayList<>();
            if(name==null&&address==null&&street==null&&subDistrict==null&&district==null&&city==null&&price==null) {
               a = apartmentRepository.findByApartmentNameOrAddressNoOrStreetOrSubDistrictOrDistrictOrCityOrRentalFeeOrFacilitiesContaining(name, address, street, subDistrict, district, city, firstPrice, fa[0]);
            }else{
                a = apartmentRepository.findByApartmentNameContainingIgnoreCaseAndAddressNoContainingIgnoreCaseAndStreetContainingIgnoreCaseAndSubDistrictContainingIgnoreCaseAndDistrictContainingIgnoreCaseAndCityContainingIgnoreCaseAndFacilitiesContaining(name, address, street, subDistrict, district, city, fa[0]);
            }
            List<Apartment> output = new ArrayList<>();
            if(firstPrice.equals(0.0) && secondPrice.equals(0.0)){

                output.addAll(a);
            }else{

                for(Apartment c:a){
                    if (c.getRentalFee() >= firstPrice && c.getRentalFee() <= secondPrice){
                        output.add(c);
                    }
                }}
            for (Apartment c : output) {
                int i = 0;
                String[] d = c.getFacilities().split(",");
                for (String f : d) {
                    for (String g : fa) {
                        if (f.equals(g)) {
                            i++;
                        }
                    }
                }
                if (i == fa.length) {
                    b.add(c);
                }
            }
                return b;
        }
        List<Apartment> tempApartment = apartmentRepository.findByApartmentNameContainingIgnoreCaseAndAddressNoContainingIgnoreCaseAndStreetContainingIgnoreCaseAndSubDistrictContainingIgnoreCaseAndDistrictContainingIgnoreCaseAndCityContainingIgnoreCase(name, address,
                street, subDistrict, district, city);
        List<Apartment> output = new ArrayList<>();
        if(firstPrice.equals(0.0) && secondPrice.equals(0.0)){
            output.addAll(tempApartment);
        }else{

        for(Apartment a:tempApartment){
            if (a.getRentalFee() >= firstPrice && a.getRentalFee() <= secondPrice){
                output.add(a);
            }
        }}
        return output;
    }

//    @Override
//    public List<Apartment> getUserByName(String name) {
//        return apartmentRepository.findByName(name, name);
//    }

    @Override
    public Apartment getApartment(Long id) {
        return apartmentRepository.findOne(id);
    }

    @Override
    public Apartment addApartment(Apartment apartment) {
        return apartmentRepository.save(apartment);
    }

    @Override
    public Apartment deleteApartment(Apartment apartment) {
        apartmentRepository.delete(apartment);
        apartment.setId(null);
        return apartment;

    }

    @Override
    public Apartment updateApartment(Apartment apartment) {
        return apartmentRepository.save(apartment);
    }

    @Override
    public List<Apartment> getApartmentsByUserId(Long id) {
        return apartmentRepository.findByUserId(id);
    }


}
