package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Message;
import camt.se331.shoppingcart.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 6/10/2558.
 */
@Repository
public class MessageDaoImlp implements MessageDao{

    @Autowired
    MessageRepository messageRepository;
    @Override
    public List<Message> getMessage() {
        return messageRepository.findAll();
    }

    @Override
    public List<Message> getMessageByUserId(Long id) {
        return messageRepository.findByUserId(id);
    }

    @Override
    public List<Message> getMessageByApartmentId(Long id) {
        return messageRepository.findByApartmentid(id);
    }



    @Override
    public List<Message> getMessageByRoomchatid(String id) {
        return messageRepository.findByRoomchatid(id);
    }

    @Override
    public Message getMessage(Long id) {
        return messageRepository.findOne(id);
    }

    @Override
    public Message addMessage(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message deleteMessage(Message message) {
        messageRepository.delete(message);
        message.setId(null);
        return message;
    }
}
