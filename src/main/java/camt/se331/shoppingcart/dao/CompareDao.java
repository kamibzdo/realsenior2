package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Compare;

import java.util.List;

/**
 * Created by Administrator on 4/10/2558.
 */
public interface CompareDao {
    List<Compare> getCompare();
    List<Compare> getCompareByUserId(Long id);
    Compare getCompare(Long id);
    Compare addCompare(Compare compare);
    Compare deleteCompare(Compare compare);


}
