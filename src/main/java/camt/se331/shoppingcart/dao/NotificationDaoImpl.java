package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Notification;
import camt.se331.shoppingcart.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 16/10/2558.
 */
@Repository
public class NotificationDaoImpl implements NotificationDao {

    @Autowired
    NotificationRepository notificationRepository;

    @Override
    public List<Notification> getNotification() {
        return notificationRepository.findAll();
    }

    @Override
    public Notification getNotification(Long id) {
        return notificationRepository.findOne(id);
    }

    @Override
    public Notification addNotification(Notification notification) {
        return notificationRepository.save(notification);
    }

    @Override
    public Notification deleteNotification(Notification notification) {
        return null;
    }
}
