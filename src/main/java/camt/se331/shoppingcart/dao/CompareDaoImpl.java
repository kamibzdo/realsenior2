package camt.se331.shoppingcart.dao;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.repository.CompareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 4/10/2558.
 */
@Repository
public class CompareDaoImpl implements CompareDao {

    @Autowired
    CompareRepository compareRepository;


    @Override
    public List<Compare> getCompare() {
        return compareRepository.findAll();
    }

    @Override
    public List<Compare> getCompareByUserId(Long id) {
        return compareRepository.findByUserId(id);
    }

    @Override
    public Compare getCompare(Long id) {
        return compareRepository.findOne(id);
    }

    @Override
    public Compare addCompare(Compare compare) {
        return compareRepository.save(compare);
    }

    @Override
    public Compare deleteCompare(Compare compare) {
        compareRepository.delete(compare);
        compare.setId(null);
        return compare;
    }
}
