package camt.se331.shoppingcart.controller;

import camt.se331.shoppingcart.entity.*;
import camt.se331.shoppingcart.service.FavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 30/9/2558.
 */
@RestController
@RequestMapping("/")
public class FavoriteController {

    @Autowired
    FavoriteService favoriteService;

    @RequestMapping(value = "Favorite", method = RequestMethod.GET)
    public List<Favorite> list() {
        return favoriteService.getFavorite();
    }

    @RequestMapping(value = "Favorite/{id}", method = RequestMethod.GET)
    public Favorite getFavorite(@PathVariable("id") Long id) {
        return favoriteService.getFavorite(id);
    }

    @RequestMapping(value = "Favorite", method = RequestMethod.POST)
    public Favorite add(@RequestBody Favorite favorite, BindingResult bindingResult) {
        return favoriteService.addFavorite(favorite);
    }

    @RequestMapping(value = "Favorite/{id}", method = RequestMethod.DELETE)
    public Favorite delete(@PathVariable("id") Long id) {

        return favoriteService.deleteFavorite(id);
    }

    @RequestMapping(value ="getFavoriteFromUser/{id}", method = RequestMethod.GET)
    public List<Favorite> getApartmentFromUser(@PathVariable("id") Long id){
        return favoriteService.getFavoriteByUserId(id);
    }

}
