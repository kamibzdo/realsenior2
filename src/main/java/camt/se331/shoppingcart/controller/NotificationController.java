package camt.se331.shoppingcart.controller;


import camt.se331.shoppingcart.entity.Notification;

import camt.se331.shoppingcart.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 16/10/2558.
 */
@RestController
@RequestMapping("/")
public class NotificationController {

    @Autowired
    NotificationService notificationService;


    @RequestMapping(value = "Notification", method = RequestMethod.GET)
    public List<Notification> list() {
        return notificationService.getNotification();
    }

    @RequestMapping(value = "Notification/{id}", method = RequestMethod.GET)
    public Notification getNotification(@PathVariable("id") Long id) {
        return notificationService.getNotification(id);
    }

    @RequestMapping(value = "Notification", method = RequestMethod.POST)
    public Notification add(@RequestBody Notification notification, BindingResult bindingResult) {

        return notificationService.addNotification(notification);

    }

    @RequestMapping(value = "Notification/{id}", method = RequestMethod.DELETE)
    public Notification delete(@PathVariable("id") Long id) {

        return notificationService.deleteNotification(id);
    }
}
