package camt.se331.shoppingcart.controller;

import camt.se331.shoppingcart.entity.Apartment;
import camt.se331.shoppingcart.entity.Photo;
import camt.se331.shoppingcart.service.ApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.Iterator;

/**
 * Created by Administrator on 27/9/2558.
 */
@Controller
@RequestMapping("/apartmentPhoto")
public class ApartmentPhotoController {

    @Autowired
    ApartmentService apartmentService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public Apartment addPhoto(HttpServletRequest request, HttpServletResponse response, @RequestParam("apartmentid") Long apartmentId) {
        MultipartHttpServletRequest mRequest;
        Apartment apartment = apartmentService.getApartment(apartmentId);
        try {
            mRequest = (MultipartHttpServletRequest) request;
            Iterator<String> itr = mRequest.getFileNames();
            while (itr.hasNext()) {
                MultipartFile multipartFile = mRequest.getFile(itr.next());
                Photo photo = new Photo();
                photo.setFileName(multipartFile.getName());
                photo.setContentType(multipartFile.getContentType());
                photo.setCreated(Calendar.getInstance().getTime());
                photo.setContent(multipartFile.getBytes());
                apartmentService.addPhoto(apartment, photo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return apartment;
    }



}
