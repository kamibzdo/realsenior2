package camt.se331.shoppingcart.controller;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Favorite;
import camt.se331.shoppingcart.service.CompareService;
import camt.se331.shoppingcart.service.FavoriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 4/10/2558.
 */
@RestController
@RequestMapping("/")
public class CompareController {
    @Autowired
    CompareService compareService;

    @RequestMapping(value = "Compare", method = RequestMethod.GET)
    public List<Compare> list() {
        return compareService.getCompare();
    }

    @RequestMapping(value = "Compare/{id}", method = RequestMethod.GET)
    public Compare getCompare(@PathVariable("id") Long id) {
        return compareService.getCompare(id);
    }

    @RequestMapping(value = "Compare", method = RequestMethod.POST)
    public Compare add(@RequestBody Compare compare, BindingResult bindingResult) {
        return compareService.addCompare(compare);
    }

    @RequestMapping(value = "Compare/{id}", method = RequestMethod.DELETE)
    public Compare delete(@PathVariable("id") Long id) {

        return compareService.deleteCompare(id);
    }

    @RequestMapping(value ="getCompareFromUser/{id}", method = RequestMethod.GET)
    public List<Compare> getCompareByUserId(@PathVariable("id") Long id){
        return compareService.getCompareByUserId(id);
    }


}
