package camt.se331.shoppingcart.controller;

import camt.se331.shoppingcart.entity.Apartment;
import camt.se331.shoppingcart.entity.Product;
import camt.se331.shoppingcart.service.ApartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 1/8/2558.
 */
@RestController
@RequestMapping("/")
public class ApartmentController {

    @Autowired
    ApartmentService apartmentService;

    @RequestMapping(value = "Apartment", method = RequestMethod.GET)
    public List<Apartment> list() {
        return apartmentService.getApartment();
    }


    @RequestMapping(value = "getApartment", method = RequestMethod.GET)
      public List<Apartment> getListByName(@RequestParam(value = "name", required = false) String name,
                                           @RequestParam(value = "address", required = false) String address,
                                           @RequestParam(value = "street", required = false) String street,
                                           @RequestParam(value = "subDistrict", required = false) String subDistrict,
                                           @RequestParam(value = "district", required = false) String district,
                                           @RequestParam(value = "city", required = false) String city,
                                           @RequestParam(value = "price", required = false) String price,
                                           @RequestParam(value = "facilities", required = false) String facilities){
        return apartmentService.getApartmentBySearch(name, address, street, subDistrict, district, city, price,facilities);
    }

    @RequestMapping(value = "Apartment", method = RequestMethod.POST)
    public
    @ResponseBody
    Apartment add(@RequestBody Apartment apartment, BindingResult bindingResult) {
        return apartmentService.addApartment(apartment);
    }

    @RequestMapping(value = "Apartment/{id}", method = RequestMethod.GET)
    public Apartment getApartment(@PathVariable("id") Long id) {
        return apartmentService.getApartment(id);
    }

    @RequestMapping(value = "Apartment/{id}", method = RequestMethod.PUT)
    public Apartment edit(@PathVariable("id") Long id, @RequestBody Apartment apartment, BindingResult bindingResult) {
        return apartmentService.updateApartment(apartment);
    }

    @RequestMapping(value = "Apartment/{id}", method = RequestMethod.DELETE)
    public Apartment delete(@PathVariable("id") Long id) {
        return apartmentService.deleteApartment(id);
    }


    @RequestMapping(value ="getApartmentFromUser/{id}", method = RequestMethod.GET)
    public List<Apartment> getApartmentFromUser(@PathVariable("id") Long id){
        return apartmentService.getApartmentByUserId(id);
    }

    @RequestMapping(value = "Apartment/photo/{id}",method = RequestMethod.PUT)
    public Apartment deletePhoto(@PathVariable("id") Long id,@RequestBody Apartment apartment, BindingResult bindingResult){
        return apartmentService.deletePhoto(apartment, id);
    }

}
