package camt.se331.shoppingcart.controller;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Message;
import camt.se331.shoppingcart.service.CompareService;
import camt.se331.shoppingcart.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 6/10/2558.
 */
@RestController
@RequestMapping("/")
public class MessageController {

    @Autowired
    MessageService messageService;

    @RequestMapping(value = "Message", method = RequestMethod.GET)
    public List<Message> list() {
        return messageService.getMessage();
    }

    @RequestMapping(value = "Message/{id}", method = RequestMethod.GET)
    public Message getMessage(@PathVariable("id") Long id) {
        return messageService.getMessage(id);
    }

    @RequestMapping(value = "Message", method = RequestMethod.POST)
    public Message add(@RequestBody Message message, BindingResult bindingResult) {
        return messageService.addMessage(message);
    }

    @RequestMapping(value = "Message/{id}", method = RequestMethod.DELETE)
    public Message delete(@PathVariable("id") Long id) {

        return messageService.deleteMessage(id);
    }

    @RequestMapping(value ="getMessageFromUser/{id}", method = RequestMethod.GET)
    public List<Message> getMessageFromUser(@PathVariable("id") Long id){
        return messageService.getMessageByUserId(id);
    }

    @RequestMapping(value ="getMessageFromApartment/{id}", method = RequestMethod.GET)
    public List<Message> getMessageFromApartment(@PathVariable("id") Long id){
        return messageService.getMessageByApartmentId(id);
    }


    @RequestMapping(value ="getMessage/{id}", method = RequestMethod.GET)
    public List<Message> getMessageByRomchatid(@PathVariable("id") String id){
        return messageService.getMessageByRoomchatid(id);
    }


}
