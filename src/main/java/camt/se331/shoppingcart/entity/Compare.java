package camt.se331.shoppingcart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by Administrator on 4/10/2558.
 */
@Entity
public class Compare {



    @Id
    @GeneratedValue
    Long id;

    Long tempApartmentid;


    @ManyToOne
    @JsonBackReference
    User user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getTempApartmentid() {
        return tempApartmentid;
    }

    public void setTempApartmentid(Long tempApartmentid) {
        this.tempApartmentid = tempApartmentid;
    }


    public Compare(){}
}
