package camt.se331.shoppingcart.entity.transfer;

import java.util.Map;

/**
 * Created by Dto on 4/20/2015.
 */
public class UserTransfer {
    private final String username;
    private final String email;
    private final String name;
    private final String password;
    private final Long id;

    private final Map<String, Boolean> roles;


    public UserTransfer(String username, String email, String name, String password, Long id, Map<String, Boolean> roles)
    {
        this.username = username;
        this.email = email;
        this.name = name;
        this.password = password;
        this.id = id;

        this.roles = roles;
    }


    public String getUsername()
    {
        return this.username;
    }


    public Map<String, Boolean> getRoles()
    {
        return this.roles;
    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }

    public Long getId() {
        return this.id;
    }
}
