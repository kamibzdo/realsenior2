package camt.se331.shoppingcart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 30/9/2558.
 */
@Entity
public class Favorite {

    @Id
    @GeneratedValue
    Long id;

    Long tempApartmentid;

    String tempapartmentName;
    String tempaddressNo;
    String tempstreet;
    String tempsubDistrict;
    String tempdistrict;
    String tempcity;
    double temprentalFee;

    @ManyToOne
    @JsonBackReference
    User user;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getTempApartmentid() {
        return tempApartmentid;
    }

    public void setTempApartmentid(Long tempApartmentid) {
        this.tempApartmentid = tempApartmentid;
    }

    public String getTempapartmentName() {
        return tempapartmentName;
    }

    public void setTempapartmentName(String tempapartmentName) {
        this.tempapartmentName = tempapartmentName;
    }

    public String getTempaddressNo() {
        return tempaddressNo;
    }

    public void setTempaddressNo(String tempaddressNo) {
        this.tempaddressNo = tempaddressNo;
    }

    public String getTempstreet() {
        return tempstreet;
    }

    public void setTempstreet(String tempstreet) {
        this.tempstreet = tempstreet;
    }

    public String getTempsubDistrict() {
        return tempsubDistrict;
    }

    public void setTempsubDistrict(String tempsubDistrict) {
        this.tempsubDistrict = tempsubDistrict;
    }

    public String getTempdistrict() {
        return tempdistrict;
    }

    public void setTempdistrict(String tempdistrict) {
        this.tempdistrict = tempdistrict;
    }

    public String getTempcity() {
        return tempcity;
    }

    public void setTempcity(String tempcity) {
        this.tempcity = tempcity;
    }

    public double getTemprentalFee() {
        return temprentalFee;
    }

    public void setTemprentalFee(double temprentalFee) {
        this.temprentalFee = temprentalFee;
    }

    Favorite(){}



}
