package camt.se331.shoppingcart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Administrator on 1/8/2558.
 */
@Entity
public class Apartment {
    public Apartment(){

    }
    @Id
    @GeneratedValue
            //1
    Long id;
    Long knowuserid;
    String apartmentName; /*ok*/
    String typeOfApartment;/*ok*/
    String contractPerson; /*ok*/
    String telephoneNumber;/*ok*/
    String email;/*ok*/
    String addressNo;/*ok*/
    String street;/*ok*/
    String subDistrict;
    String district;/*ok*/
    String city;/*ok*/
    String zipCode;/*ok*/
    //2
    String facilities; /*atmost*/
    String typeOfRoom;/*ok*/
    String numberOfBedroom;/*ok*/
    String roomsize;/*ok*/
    //3
    double rentalFee;/*ok*/
    int deposit;/*ok*/
    int numberOfMonthForprepaid;/*ok*/
    int electricFee;/*ok*/
    int waterFee;/*ok*/
    int telephoneFee;/*ok*/
    int internetFee;/*ok*/
    String typeOfInternet;
    String detail;

    @ManyToOne
    @JsonBackReference
    User user;

//    @ManyToOne
//    @JsonBackReference
//    Favorite favorite;
//
//    public Favorite getFavorite() {
//        return favorite;
//    }
//
//    public void setFavorite(Favorite favorite) {
//        this.favorite = favorite;
//    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    Set<Photo> photos = new HashSet<>();


    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    public Apartment(String apartmentName, String typeOfApartment, String contractPerson, String telephoneNumber, String email, String addressNo, String street, String subDistrict, String district, String city, String zipCode, String facilities, String typeOfRoom, String numberOfBedroom, String roomsize, double rentalFee, int deposit, int numberOfMonthForprepaid, int electricFee, int waterFee, int telephoneFee, int internetFee, String typeOfInternet, String detail) {
        this.apartmentName = apartmentName;
        this.typeOfApartment = typeOfApartment;
        this.contractPerson = contractPerson;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.addressNo = addressNo;
        this.street = street;
        this.subDistrict = subDistrict;
        this.district = district;
        this.city = city;
        this.zipCode = zipCode;
        this.facilities = facilities;
        this.typeOfRoom = typeOfRoom;
        this.numberOfBedroom = numberOfBedroom;
        this.roomsize = roomsize;
        this.rentalFee = rentalFee;
        this.deposit = deposit;
        this.numberOfMonthForprepaid = numberOfMonthForprepaid;
        this.electricFee = electricFee;
        this.waterFee = waterFee;
        this.telephoneFee = telephoneFee;
        this.internetFee = internetFee;
        this.typeOfInternet = typeOfInternet;
        this.detail = detail;
    }

    public Apartment(String apartmentName, String typeOfApartment, String contractPerson, String telephoneNumber, String email, String addressNo, String street, String subDistrict, String district, String city, String zipCode, String facilities, String typeOfRoom, String numberOfBedroom, String roomsize, double rentalFee, int deposit, int numberOfMonthForprepaid, int electricFee, int waterFee, int telephoneFee, int internetFee, String typeOfInternet, String detail, Photo photo) {
        this.apartmentName = apartmentName;
        this.typeOfApartment = typeOfApartment;
        this.contractPerson = contractPerson;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.addressNo = addressNo;
        this.street = street;
        this.subDistrict = subDistrict;
        this.district = district;
        this.city = city;
        this.zipCode = zipCode;
        this.facilities = facilities;
        this.typeOfRoom = typeOfRoom;
        this.numberOfBedroom = numberOfBedroom;
        this.roomsize = roomsize;
        this.rentalFee = rentalFee;
        this.deposit = deposit;
        this.numberOfMonthForprepaid = numberOfMonthForprepaid;
        this.electricFee = electricFee;
        this.waterFee = waterFee;
        this.telephoneFee = telephoneFee;
        this.internetFee = internetFee;
        this.typeOfInternet = typeOfInternet;
        this.detail = detail;
        this.getPhotos().add(photo);
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApartmentName() {
        return apartmentName;
    }

    public void setApartmentName(String apartmentName) {
        this.apartmentName = apartmentName;
    }

    public String getTypeOfApartment() {
        return typeOfApartment;
    }

    public void setTypeOfApartment(String typeOfApartment) {
        this.typeOfApartment = typeOfApartment;
    }

    public String getContractPerson() {
        return contractPerson;
    }

    public void setContractPerson(String contractPerson) {
        this.contractPerson = contractPerson;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddressNo() {
        return addressNo;
    }

    public void setAddressNo(String addressNo) {
        this.addressNo = addressNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getFacilities() {
        return facilities;
    }

    public void setFacilities(String facilities) {
        this.facilities = facilities;
    }

    public String getTypeOfRoom() {
        return typeOfRoom;
    }

    public void setTypeOfRoom(String typeOfRoom) {
        this.typeOfRoom = typeOfRoom;
    }

    public String getNumberOfBedroom() {
        return numberOfBedroom;
    }

    public void setNumberOfBedroom(String numberOfBedroom) {
        this.numberOfBedroom = numberOfBedroom;
    }

    public String getRoomsize() {
        return roomsize;
    }

    public void setRoomsize(String roomsize) {
        this.roomsize = roomsize;
    }

    public double getRentalFee() {
        return rentalFee;
    }

    public void setRentalFee(double rentalFee) {
        this.rentalFee = rentalFee;
    }

    public int getDeposit() {
        return deposit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public int getNumberOfMonthForprepaid() {
        return numberOfMonthForprepaid;
    }

    public void setNumberOfMonthForprepaid(int numberOfMonthForprepaid) {
        this.numberOfMonthForprepaid = numberOfMonthForprepaid;
    }

    public int getElectricFee() {
        return electricFee;
    }

    public void setElectricFee(int electricFee) {
        this.electricFee = electricFee;
    }

    public int getWaterFee() {
        return waterFee;
    }

    public void setWaterFee(int waterFee) {
        this.waterFee = waterFee;
    }

    public int getTelephoneFee() {
        return telephoneFee;
    }

    public void setTelephoneFee(int telephoneFee) {
        this.telephoneFee = telephoneFee;
    }

    public int getInternetFee() {
        return internetFee;
    }

    public void setInternetFee(int internetFee) {
        this.internetFee = internetFee;
    }

    public String getTypeOfInternet() {
        return typeOfInternet;
    }

    public void setTypeOfInternet(String typeOfInternet) {
        this.typeOfInternet = typeOfInternet;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Long getKnowuserid() {
        return knowuserid;
    }

    public void setKnowuserid(Long knowuserid) {
        this.knowuserid = knowuserid;
    }
}
