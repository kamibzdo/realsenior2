package camt.se331.shoppingcart.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Dto on 4/19/2015.
 */
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    public String username;
    public String name;
    public String email;
    public String password;
    public Date dob;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    //    @ManyToOne
//    @Cascade(CascadeType.ALL)
    @Transient
    public Role role;

//    @Transient
//    public Apartment apartments;

    @OneToMany(fetch= FetchType.EAGER)
    // Cascade and CascadeType must be the org.hibernate.annotation
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    public Set<Apartment> apartments = new HashSet<>();

    public Set<Apartment> getApartments() {
        return apartments;
    }

    public void setApartments(Set<Apartment> apartments) {
        this.apartments = apartments;
    }

//    @ManyToMany
//    @Cascade(CascadeType.ALL)
//    public UserAuthenticationController use;
//    public UserAuthenticationController getUse() {
//        return use;
//    }

//    public void setUse(UserAuthenticationController use) {
//        this.use = use;
//    }
//    public String choose;

//    @Autowired
//    UserRepository userRepository;

    @ManyToMany(fetch= FetchType.EAGER)
    // Cascade and CascadeType must be the org.hibernate.annotation
    @Cascade(CascadeType.ALL)
    public Set<Role> roles = new HashSet<>();

    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    private Set<Favorite> favorites = new HashSet<>();

    public Set<Favorite> getFavorites() {
        return favorites;
    }

    public void setFavorites(Set<Favorite> favorites) {
        this.favorites = favorites;
    }


    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    private  Set<Compare> compares = new HashSet<>();

    public Set<Compare> getCompares() {
        return compares;
    }

    public void setCompares(Set<Compare> compares) {
        this.compares = compares;
    }

    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    private  Set<Chat> chats = new HashSet<>();

    public Set<Chat> getChats() {
        return chats;
    }

    public void setChats(Set<Chat> chats) {
        this.chats = chats;
    }

    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    private  Set<Message> messages = new HashSet<>();

    public Set<Message> getMessages() {
        return messages;
    }

    public void setMessages(Set<Message> messages) {
        this.messages = messages;
    }

    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    private Set<ShoppingCart> shoppingCarts = new HashSet<>();

//    User user = new User();

    public Set<ShoppingCart> getShoppingCarts() {
        return shoppingCarts;
    }

    public void setShoppingCarts(Set<ShoppingCart> shoppingCarts) {
        this.shoppingCarts = shoppingCarts;
    }

    @OneToMany(fetch= FetchType.EAGER)
    @Cascade(CascadeType.ALL)
    @JsonManagedReference
    private Set<Notification> notifications = new HashSet<>();

    public Set<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(Set<Notification> notifications) {
        this.notifications = notifications;
    }

    public User() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (dob != null ? !dob.equals(user.dob) : user.dob != null) return false;
        return !(roles != null ? !roles.equals(user.roles) : user.roles != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (dob != null ? dob.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        return result;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User(Long id, String name,String username, String email, String password) {
        this.id = id;
        this.name = name;
        this.username = username;

        this.email = email;
        this.password = password;
        this.dob = dob;
    }

    public User(String username, String name, String email, String password) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = password;
        getRoles().add(new Role("user"));
    }

    public User(String username, String name, String email, String password, Set<Role> roles) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = roles;
        getRoles().add(new Role("user"));
    }

    public User(Long id, String username, String name, String email, String password, Set<Role> roles) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = roles;
        getRoles().add(new Role("user"));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

//    public void setRose(String choose){
////        this.choose = choose;
////        userrole = new Role(this.choose);
////        roles.add(userrole);
////        user.setRoles(roles);
////        userRepository.save(user);
//
//
//    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", dob=" + dob +
                ", roles=" + roles +
                '}';
    }
}
