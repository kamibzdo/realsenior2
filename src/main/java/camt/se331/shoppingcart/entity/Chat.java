package camt.se331.shoppingcart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by Administrator on 11/10/2558.
 */
@Entity
public class Chat {


    @Id
    @GeneratedValue
    Long id;

    Long toid;
    Long fromid;
    Long roomchatid;

    String temp;


    @ManyToOne
    @JsonBackReference
    User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getToid() {
        return toid;
    }

    public void setToid(Long toid) {
        this.toid = toid;
    }

    public Long getFromid() {
        return fromid;
    }

    public void setFromid(Long fromid) {
        this.fromid = fromid;
    }

    public Long getRoomchatid() {
        return roomchatid;
    }

    public void setRoomchatid(Long roomchatid) {
        this.roomchatid = roomchatid;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public Chat(){}
}
