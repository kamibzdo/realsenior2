package camt.se331.shoppingcart.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by Administrator on 6/10/2558.
 */
@Entity
public class Message {

    Message() {
    }

    @Id
    @GeneratedValue
    Long id;

    String messageContent;

    String nameofuser;

    Long apartmentid;

    Long userid;

    String roomchatid;

    Long replyid;

    String replymessage;

    String date;

    @ManyToOne
    @JsonBackReference
    User user;

   


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }




    public String getNameofuser() {
        return nameofuser;
    }

    public void setNameofuser(String nameofuser) {
        this.nameofuser = nameofuser;
    }

    public Long getApartmentid() {
        return apartmentid;
    }

    public void setApartmentid(Long apartmentid) {
        this.apartmentid = apartmentid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getRoomchatid() {
        return roomchatid;
    }

    public void setRoomchatid(String roomchatid) {
        this.roomchatid = roomchatid;
    }

    public Long getReplyid() {
        return replyid;
    }

    public void setReplyid(Long replyid) {
        this.replyid = replyid;
    }

    public String getReplymessage() {
        return replymessage;
    }

    public void setReplymessage(String replymessage) {
        this.replymessage = replymessage;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Message(String messageContent, String nameofuser, Long apartmentid, Long userid, String roomchatid) {
        this.messageContent = messageContent;
        this.nameofuser = nameofuser;
        this.apartmentid = apartmentid;
        this.userid = userid;
        this.roomchatid = roomchatid;
    }

    public Message(String messageContent, String nameofuser, Long apartmentid, Long userid, String roomchatid, Long replyid, String replymessage) {
        this.messageContent = messageContent;
        this.nameofuser = nameofuser;
        this.apartmentid = apartmentid;
        this.userid = userid;
        this.roomchatid = roomchatid;
        this.replyid = replyid;
        this.replymessage = replymessage;
    }

    public Message(String messageContent, String nameofuser, Long apartmentid, Long userid, String roomchatid, Long replyid, String replymessage, String date) {
        this.messageContent = messageContent;
        this.nameofuser = nameofuser;
        this.apartmentid = apartmentid;
        this.userid = userid;
        this.roomchatid = roomchatid;
        this.replyid = replyid;
        this.replymessage = replymessage;
        this.date = date;
    }
}
