package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Apartment;
import camt.se331.shoppingcart.entity.Image;
import camt.se331.shoppingcart.entity.Photo;
import camt.se331.shoppingcart.entity.Product;

import java.util.List;

/**
 * Created by Administrator on 1/8/2558.
 */
public interface ApartmentService {
    List<Apartment> getApartment();
    List<Apartment> getApartmentBySearch(String name,String address,String street,String subDistrict,String district,String city,String price,String facilities);
    Apartment getApartment(Long id);
    Apartment addApartment(Apartment apartment);
    Apartment deleteApartment(Long id);
    Apartment updateApartment(Apartment apartment);
    List<Apartment> getApartmentByUserId(Long id);
//    List<Apartment> getApartmentByName(String name);


    Apartment addPhoto(Apartment apartment, Photo photo);
    Apartment deletePhoto(Apartment apartment,long photoNumber);
}

