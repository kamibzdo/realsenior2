package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.ChatDao;
import camt.se331.shoppingcart.entity.Chat;
import camt.se331.shoppingcart.entity.Compare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Administrator on 11/10/2558.
 */
@Service
public class ChatServiceImlp implements ChatService {

    @Autowired
    ChatDao chatDao;

    public ChatServiceImlp(){}
    public ChatServiceImlp(ChatDao chatDao) {
        this.chatDao = chatDao;
    }

    @Override
    public List<Chat> getChat() {
        return chatDao.getChat();
    }

    @Override
    public List<Chat> getChatByUserId(Long id) {
        return chatDao.getChatByUserId(id);
    }

    @Override
    public List<Chat> getChatByChatroomid(Long id) {
        return chatDao.getChatByChatroomid(id);
    }

    @Override
    public Chat getChat(Long id) {
        return chatDao.getChat(id);
    }

    @Override
    public Chat addChat(Chat chat) {
        return chatDao.addChat(chat);
    }

    @Override
    @Transactional
    public Chat deleteChat(Long id) {
        Chat chat = getChat(id);
//        chat.getUser().getChats().remove(chat);
        return chatDao.deleteChat(chat);
    }
}
