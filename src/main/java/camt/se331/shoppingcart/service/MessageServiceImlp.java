package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.MessageDao;
import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 6/10/2558.
 */
@Service
public class MessageServiceImlp implements MessageService{

    @Autowired
    MessageDao messageDao;
    public MessageServiceImlp(){}
    public MessageServiceImlp(MessageDao messageDao){
        this.messageDao = messageDao;
    }
    @Override
    public List<Message> getMessage() {
        return messageDao.getMessage();
    }

    @Override
    public List<Message> getMessageByUserId(Long id) {
        return messageDao.getMessageByUserId(id);
    }

    @Override
    public List<Message> getMessageByApartmentId(Long id) {
        return messageDao.getMessageByApartmentId(id);
    }



    @Override
    public List<Message> getMessageByRoomchatid(String id) {
        return messageDao.getMessageByRoomchatid(id);
    }

    @Override
    public Message getMessage(Long id) {
        return messageDao.getMessage(id);
    }

    @Override
    public Message addMessage(Message message) {
        return messageDao.addMessage(message);
    }

    @Override
    @Transactional
    public Message deleteMessage(Long id) {
        Message message = getMessage(id);
//        message.getUser().getCompares().remove(message);
//        when(message.getUser().getCompares().remove(message)).thenReturn();
        return messageDao.deleteMessage(message);
    }


}
