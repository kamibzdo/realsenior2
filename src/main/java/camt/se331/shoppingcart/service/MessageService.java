package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Message;

import java.util.List;

/**
 * Created by Administrator on 6/10/2558.
 */
public interface MessageService {

    List<Message> getMessage();
    List<Message> getMessageByUserId(Long id);
    List<Message> getMessageByApartmentId(Long id);

    List<Message> getMessageByRoomchatid(String id);
    Message getMessage(Long id);
    Message addMessage(Message message);
    Message deleteMessage(Long id);

}
