package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.NotificationDao;
import camt.se331.shoppingcart.entity.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 16/10/2558.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

    @Autowired
    NotificationDao notificationDao;

    @Override
    public List<Notification> getNotification() {
        return notificationDao.getNotification();
    }

    @Override
    public Notification getNotification(Long id) {
        return notificationDao.getNotification(id);
    }

    @Override
    public Notification addNotification(Notification notification) {
        return notificationDao.addNotification(notification);
    }

    @Override
    public Notification deleteNotification(Long id) {
        return null;
    }
}
