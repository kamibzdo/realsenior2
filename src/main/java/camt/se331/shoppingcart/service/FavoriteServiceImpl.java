package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.FavoriteDao;
import camt.se331.shoppingcart.entity.Favorite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Administrator on 1/10/2558.
 */
@Service
public class FavoriteServiceImpl implements FavoriteService {

    @Autowired
    FavoriteDao favoriteDao;

    public FavoriteServiceImpl(){}

    public FavoriteServiceImpl(FavoriteDao favoriteDao) {
        this.favoriteDao = favoriteDao;
    }


    @Override
    public List<Favorite> getFavorite() {
        return favoriteDao.getFavorite();
    }

    @Override
    public List<Favorite> getFavoriteByUserId(Long id) {
        return favoriteDao.getFavoriteByUserId(id);
    }

    @Override
    public Favorite getFavorite(Long id) {
        return favoriteDao.getFavorite(id);
    }

    @Override
    public Favorite addFavorite(Favorite favorite) {
        return favoriteDao.addFavorite(favorite);
    }

    @Override
    @Transactional
    public Favorite deleteFavorite(Long id) {
        Favorite favorite = getFavorite(id);

        return favoriteDao.deleteFavorite(favorite);
    }


}
