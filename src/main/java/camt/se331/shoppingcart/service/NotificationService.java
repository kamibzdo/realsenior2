package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Chat;
import camt.se331.shoppingcart.entity.Notification;

import java.util.List;

/**
 * Created by Administrator on 16/10/2558.
 */
public interface NotificationService {

    List<Notification> getNotification();
//    List<Notification> getChatByUserId(Long id);
//    List<Notification> getChatByChatroomid(String id);
    Notification getNotification(Long id);
    Notification addNotification(Notification notification);
    Notification deleteNotification(Long id);
}
