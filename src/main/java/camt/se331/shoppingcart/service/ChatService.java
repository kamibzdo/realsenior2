package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Chat;


import java.util.List;

/**
 * Created by Administrator on 11/10/2558.
 */

public interface ChatService {

    List<Chat> getChat();
    List<Chat> getChatByUserId(Long id);
    List<Chat> getChatByChatroomid(Long id);
    Chat getChat(Long id);
    Chat addChat(Chat chat);
    Chat deleteChat(Long id);

}
