package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Favorite;

import java.util.List;

/**
 * Created by Administrator on 4/10/2558.
 */
public interface CompareService {
    List<Compare> getCompare();
    List<Compare> getCompareByUserId(Long id);
    Compare getCompare(Long id);
    Compare addCompare(Compare compare);
    Compare deleteCompare(Long id);
}
