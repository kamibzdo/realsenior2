package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Favorite;

import java.util.List;

/**
 * Created by Administrator on 1/10/2558.
 */
public interface FavoriteService {
    List<Favorite> getFavorite();
    List<Favorite> getFavoriteByUserId(Long id);
    Favorite getFavorite(Long id);
    Favorite addFavorite(Favorite favorite);
    Favorite deleteFavorite(Long id);

}
