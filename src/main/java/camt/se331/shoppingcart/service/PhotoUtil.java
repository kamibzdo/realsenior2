package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.entity.Image;
import camt.se331.shoppingcart.entity.Photo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;

/**
 * Created by Administrator on 27/9/2558.
 */
public class PhotoUtil {

    static PhotoUtil photoUtil = null;
    public static PhotoUtil getInstance(){
        if (photoUtil == null){
            photoUtil = new PhotoUtil();
        }
        return photoUtil;
    }
    public static Photo getPhoto(String resourcePath){
        Photo photo = new Photo();
        ClassLoader classLoader = PhotoUtil.getInstance().getClass().getClassLoader();

        File file = new File(classLoader.getResource(resourcePath).getFile());

        try {

            photo.setFileName(file.getName());
            photo.setContentType(Files.probeContentType(file.toPath()));
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            for (int readNum; (readNum = fis.read(buf)) != -1;){
                bos.write(buf,0,readNum);
            }
            photo.setContent(bos.toByteArray());
            photo.setCreated(Calendar.getInstance().getTime());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;

    }

}
