package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.CompareDao;
import camt.se331.shoppingcart.dao.FavoriteDao;
import camt.se331.shoppingcart.entity.Compare;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Administrator on 4/10/2558.
 */
@Service
public class CompareServiceImlp implements CompareService {

    @Autowired
    CompareDao compareDao;

    public CompareServiceImlp(){}

    public CompareServiceImlp(CompareDao compareDao) {
        this.compareDao = compareDao;
    }

    @Override
    public List<Compare> getCompare() {
      return compareDao.getCompare();
    }

    @Override
    public List<Compare> getCompareByUserId(Long id) {
        return compareDao.getCompareByUserId(id);
    }

    @Override
    public Compare getCompare(Long id) {
        return compareDao.getCompare(id);
    }

    @Override
    public Compare addCompare(Compare compare) {
        return compareDao.addCompare(compare);
    }

    @Override
    @Transactional
    public Compare deleteCompare(Long id) {

        Compare compare = getCompare(id);
//        compare.getUser().getCompares().remove(compare);
        return compareDao.deleteCompare(compare);
    }
}
