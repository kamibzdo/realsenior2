package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.ApartmentDao;
import camt.se331.shoppingcart.dao.UserDao;
import camt.se331.shoppingcart.entity.Apartment;
import camt.se331.shoppingcart.entity.*;
import camt.se331.shoppingcart.repository.ImageRepository;
import camt.se331.shoppingcart.repository.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Administrator on 1/8/2558.
 */
@Service
public class ApartmentServiceImpl implements ApartmentService {

    @Autowired
    ApartmentDao apartmentDao;
    @Autowired
    UserDao userDao;
    @Autowired
    PhotoRepository photoRepository;

    public ApartmentServiceImpl(){}

    public ApartmentServiceImpl(ApartmentDao apartmentDao) {
        this.apartmentDao = apartmentDao;
    }

    @Override
    public List<Apartment> getApartment() {
        return apartmentDao.getApartment();
    }

    @Override
    public List<Apartment> getApartmentBySearch(String name,String address,String street,String subDistrict,String district,String city,String price,String facilities)
    {
        return apartmentDao.getApartmentBySearch(name,address,street,subDistrict,district,city,price,facilities);
    }

    @Override
    public Apartment getApartment(Long id) {
        return apartmentDao.getApartment(id);
    }

    @Override
    @Transactional
    public Apartment addApartment(Apartment apartment) {
        User user = userDao.getUser(apartment.getUser().getId());
        apartment.setUser(user);
        user.getApartments().add(apartment);

        return apartmentDao.addApartment(apartment);
    }

    @Override
    @Transactional
    public Apartment deleteApartment(Long id) {
        Apartment apartment = getApartment(id);
//        apartment.getUser().getApartments().remove(apartment);
        return apartmentDao.deleteApartment(apartment);
    }

    @Override
    public Apartment updateApartment(Apartment apartment) {
        Apartment apartment1 = apartmentDao.getApartment(apartment.getId());
        apartment.setPhotos(apartment1.getPhotos());
        return apartmentDao.updateApartment(apartment);
    }

    @Override
    public List<Apartment> getApartmentByUserId(Long id) {
        return apartmentDao.getApartmentsByUserId(id);
    }

//    @Override
//    public List<Apartment> getApartmentByName(String name) {
//        return apartmentDao.getUserByName(name);
//    }

    @Override
    @Transactional
    public Apartment addPhoto(Apartment apartment, Photo photo) {
        apartment.getPhotos().add(photo);
        apartmentDao.updateApartment(apartment);
        return apartment;
    }
    @Override
    @Transactional
    public Apartment deletePhoto(Apartment apartment,long photoNumber) {
        Photo photo = photoRepository.findOne(photoNumber);
        Apartment apartment1 = apartmentDao.getApartment(apartment.getId());
        List<Photo> a = new ArrayList<Photo>();
        if(apartment1.getPhotos()!=null) {
            for (Photo str : apartment1.getPhotos()) {
                a.add(str);
            }
        }
        for(int i=0;i<a.size();i++) {
            if(a.get(i).getId()==photo.getId()) {
                a.remove(i);
            }
        }
        apartment1.getPhotos().clear();
        apartment1.setPhotos(new HashSet<Photo>(a));
        apartmentDao.updateApartment(apartment1);
        photoRepository.delete(photoNumber);
        return  apartment;
    }
}
