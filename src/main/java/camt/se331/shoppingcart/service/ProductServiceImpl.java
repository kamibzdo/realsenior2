package camt.se331.shoppingcart.service;

import camt.se331.shoppingcart.dao.ProductDao;
import camt.se331.shoppingcart.entity.Image;
import camt.se331.shoppingcart.entity.Product;
import camt.se331.shoppingcart.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Dto on 2/8/2015.
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDao productDao;
    @Autowired
    ImageRepository imageRepository;
    @Override
    public List<Product> getProducts() {
        return productDao.getProducts();
    }

    @Override
    public Product getProduct(Long id) {
        return productDao.getProduct(id);
    }

    @Override
    public Product addProduct(Product product) {
        return productDao.addProduct(product);
    }

    @Override
    public Product deleteProduct(Long id) {
        Product product = getProduct(id);
        return productDao.deleteProduct(product);
    }

    @Override
    public Product updateProduct(Product product) {
        Product product1 = productDao.getProduct(product.getId());
        product.setImages(product1.getImages());
        return productDao.updateProduct(product);
    }

    @Override
    public List<Product> getProductsByName(String name) {
        return productDao.getProductsByName(name);
    }

    @Override
    public List<Product> getProductsByNew(String name,String description,Double price) {
        return productDao.getProductsByNew(name,description,price);
    }

    @Override
    @Transactional
    public Product addImage(Product product, Image image) {
        product.getImages().add(image);
        productDao.updateProduct(product);
        return product;
    }
    @Override
    @Transactional
    public Product deleteImage(Product product,long imageNumber) {
        Image image = imageRepository.findOne(imageNumber);
        Product product1 = productDao.getProduct(product.getId());
        List<Image> a = new ArrayList<Image>();
        if(product1.getImages()!=null) {
            for (Image str : product1.getImages()) {
                a.add(str);
            }
        }
        for(int i=0;i<a.size();i++) {
            if(a.get(i).getId()==image.getId()) {
                a.remove(i);
            }
        }
        product1.getImages().clear();
        product1.setImages(new HashSet<Image>(a));
        productDao.updateProduct(product1);
        imageRepository.delete(imageNumber);
        return  product;
    }

}
