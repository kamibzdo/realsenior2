package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 6/10/2558.
 */
public interface MessageRepository extends JpaRepository<Message,Long>{

    List<Message> findByUserId(Long id);
    List<Message> findByApartmentid(Long id);
    List<Message> findByReplyid(Long id);
    List<Message> findByRoomchatid(String id);

}
