package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 27/9/2558.
 */
public interface PhotoRepository extends JpaRepository<Photo,Long> {
}
