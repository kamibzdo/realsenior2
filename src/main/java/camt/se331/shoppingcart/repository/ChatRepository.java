package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Chat;
import camt.se331.shoppingcart.entity.Compare;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 11/10/2558.
 */
public interface ChatRepository extends JpaRepository<Chat,Long> {

    List<Chat> findByUserId(Long id);
    List<Chat> findByRoomchatid(Long id);
}
