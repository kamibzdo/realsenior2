package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Administrator on 16/10/2558.
 */
public interface NotificationRepository extends JpaRepository<Notification,Long>{


}
