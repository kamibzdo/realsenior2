package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dell on 15/5/2558.
 */
public interface ImageRepository extends JpaRepository<Image,Long> {
}
