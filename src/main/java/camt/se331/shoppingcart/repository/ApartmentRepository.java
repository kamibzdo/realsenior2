package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Apartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Administrator on 1/8/2558.
 */
public interface ApartmentRepository extends JpaRepository<Apartment, Long> {
    //    public List<Apartment> findByNameLike(String name);
//    public List<Apartment> findByNameContaining(String name);
//    public List<Apartment> findByNameContainingIgnoreCase(String name);
    public List<Apartment> findByApartmentNameOrAddressNoOrStreetOrSubDistrictOrDistrictOrCityOrRentalFee(String name, String address, String street, String subDistrict, String district, String city, Double price);

    List<Apartment> findByUserId(Long id);

   public List<Apartment> findByApartmentNameContainingIgnoreCaseAndAddressNoContainingIgnoreCaseAndStreetContainingIgnoreCaseAndSubDistrictContainingIgnoreCaseAndDistrictContainingIgnoreCaseAndCityContainingIgnoreCaseAndFacilitiesContaining(String name, String address, String street, String subDistrict, String district, String city,String facilities);
    public List<Apartment> findByApartmentNameOrAddressNoOrStreetOrSubDistrictOrDistrictOrCityOrRentalFeeOrFacilitiesContaining(String name, String address, String street, String subDistrict, String district, String city, Double price, String facilities);

    List<Apartment> findByApartmentNameAndAddressNoAndStreetAndSubDistrictAndDistrictAndCityAndRentalFee(String name, String address, String street, String subDistrict, String district, String city, Double price);

    List<Apartment> findByApartmentNameContainingIgnoreCaseAndAddressNoContainingIgnoreCaseAndStreetContainingIgnoreCaseAndSubDistrictContainingIgnoreCaseAndDistrictContainingIgnoreCaseAndCityContainingIgnoreCase(String name, String address, String street, String subDistrict, String district, String city);
    List<Apartment> findByApartmentNameLikeOrAddressNoOrStreetOrSubDistrictOrDistrictLikeOrCityLikeAndRentalFeeLessThanEqualAndRentalFeeLessThanEqual(String name, String address, String street, String subDistrict, String district, String city, Double fristPrice,Double lastPrice);
//    @Query(value="select a from Apartment a WHERE " +
//            "(LOWER(a.apartmentName) = " + "%" + "LOWER(:name)"+"%"+ " OR " +
//            "LOWER(a.addressNo) = "  + "%" + "LOWER(:address)" +"%"+ " ) AND " +
//            "(a.rentalFee >= :firstPrice and a.rentalFee <= :secondPrice)"
//    )
//    List<Apartment> searchApartment(@Param("name")String name,@Param("address") String address,ContainingIgnoreCase
//                                    @Param("firstPrice")Double fristPrice,@Param("secondPrice")Double lastPrice);
        }
