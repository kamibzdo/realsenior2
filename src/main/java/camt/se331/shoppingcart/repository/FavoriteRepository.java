package camt.se331.shoppingcart.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import camt.se331.shoppingcart.entity.Favorite;

import java.util.List;


/**
 * Created by Administrator on 1/10/2558.
 */
public interface FavoriteRepository extends JpaRepository<Favorite,Long>{

    List<Favorite> findByUserId(Long id);
}
