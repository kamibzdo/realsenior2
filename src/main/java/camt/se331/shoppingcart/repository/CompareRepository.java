package camt.se331.shoppingcart.repository;

import camt.se331.shoppingcart.entity.Compare;
import camt.se331.shoppingcart.entity.Favorite;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Administrator on 4/10/2558.
 */
public interface CompareRepository extends JpaRepository<Compare,Long> {


    List<Compare> findByUserId(Long id);
}
