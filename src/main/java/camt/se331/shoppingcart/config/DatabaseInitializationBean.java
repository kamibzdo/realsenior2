package camt.se331.shoppingcart.config;

import camt.se331.shoppingcart.entity.*;
import camt.se331.shoppingcart.repository.*;
import camt.se331.shoppingcart.service.ImageUtil;
import camt.se331.shoppingcart.service.PhotoUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by Dto on 2/11/2015.
 */
@Component
@Profile("db.init")
public class DatabaseInitializationBean implements InitializingBean {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ShoppingCartRepository shoppingCartRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ApartmentRepository apartmentRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
        Product[] initProduct = {
                new Product(1l, "Kindle", "the good book reader", 6900.00, ImageUtil.getImage("pic/x.png")),
                new Product(2l, "Surface Pro", "The unknow computer", 34000.00, ImageUtil.getImage("pic/x.png")),
                new Product(3l, "Mac pro", " Mac book interim", 44000.00, ImageUtil.getImage("pic/x.png")),
                new Product(4l, "Candle", "use for lightenup the world", 10.00, ImageUtil.getImage("pic/x.png")),
                new Product(5l, "Bin", "User for what ?", 200.00, ImageUtil.getImage("pic/x.png")),
                new Product(6l, "Telephone", "Call the others", 150.00, ImageUtil.getImage("pic/x.png")),
                new Product(7l, "iPhone", "What is it?", 26000.00, ImageUtil.getImage("pic/x.png")),
                new Product(8l, "Galaxy Note 4", "Who still use this ?", 24000.00, ImageUtil.getImage("pic/x.png")),
                new Product(9l, "AngularJS", "we hate it", 2000.00, ImageUtil.getImage("pic/x.png")),
                new Product(10l, "Mazda 3", "Very handsome guy use this", 300000.00, ImageUtil.getImage("pic/x.png"))
        };

        productRepository.save(Arrays.asList(initProduct));
//        productRepository.save(new Product(1l,"Kindle","the good book reader",6900.00));

        Apartment[] initApartment = {
                new Apartment("CAMTResidence", "Apartment", "Kachapon", "054360555", "kachapon-21@hotmail.com", "239", "Huay Khew", "Suthep", "Muang", "Chiang Mai", "50200", "Microwave,Keycard,Swimmingpool,Fan,", "Deluxe", "2 bedrooms", "32.00", 5000, 5000, 2, 7, 20, 5, 400, "Wi-Fi", "Luxury apartment serves monthly. Enjoy in house facilities restaurant, laundry shop minimart, 24 hours front desk service",PhotoUtil.getPhoto("pic/apartment-room-8.jpg")),
                new Apartment("Thong Rin Place", "Apartment", "Kachapon", "054360555", "kachapon-21@hotmail.com", "283", "Huay Khew", "Suthep", "Mea Rim", "Chiang Mai", "50200", "Fan,", "Deluxe", "2 bedrooms", "32.00", 5000, 5000, 2, 7, 20, 5, 400, "Wi-Fi", "Luxury apartment serves monthly. Enjoy in house facilities restaurant, laundry shop minimart, 24 hours front desk service", PhotoUtil.getPhoto("pic/Modern-Living-Room-Furniture-Apartment-With-Cool.jpg")),
                new Apartment("Nobel", "Serviced apartment", "Kachapon", "054360555", "kachapon-21@hotmail.com", "139", "Huay Khew", "Suthep", "Muang", "Chiang Mai", "50200", "Microwave,", "Deluxe", "2 bedrooms", "32.00", 7500, 5000, 2, 7, 20, 5, 400, "Wi-Fi", "Luxury apartment serves monthly. Enjoy in house facilities restaurant, laundry shop minimart, 24 hours front desk service",PhotoUtil.getPhoto("pic/Russian-Apartment-Luxury-Interior-Design.jpg")),
                new Apartment("Sweet House", "apartment", "Kachapon", "054360555", "kachapon-21@hotmail.com", "039", "Huay Khew", "Suthep", "Muang", "Chiang Mai", "50200", "Aircondition,", "Deluxe", "2 bedrooms", "32.00", 5000, 7500, 2, 7, 20, 5, 400, "LAN", "Luxury apartment serves monthly. Enjoy in house facilities restaurant, laundry shop minimart, 24 hours front desk service",PhotoUtil.getPhoto("pic/Small-apartment-living-room-ideas-with-minimalist-.jpg")),
//                new Apartment("apartmentName", "typeOfApartment", "contractPerson", "telephoneNumber", "email", "addressNo", "street", "subDistrict", "district", "city", "zipCode", "facilities", "typeOfRoom", "numberOfBedroom", "roomsize", rentalfee,  deposit, numberOfMonthForprepaid, electricFee, waterFee,telephoneFee ,internetFee, "typeOfInternet", "detail")
        };

        apartmentRepository.save(Arrays.asList(initApartment));


//        ShoppingCart shoppingCart = new ShoppingCart();
//        List<SelectedProduct> selectedProducts = new ArrayList<>();
//        SelectedProduct[] initSelectedProduct = {
//                new SelectedProduct(initProduct[2], 5),
//                new SelectedProduct(initProduct[4], 2),
//                new SelectedProduct(initProduct[1], 1),
//        };
//        selectedProducts.addAll(Arrays.asList(initSelectedProduct));
//        Calendar calendar = new GregorianCalendar(2015,4,7);
//        shoppingCart.setSelectedProducts(selectedProducts);
//        shoppingCart.setPurchaseDate(calendar.getTime());
//        shoppingCart.setId(1L);
//        shoppingCartRepository.save(shoppingCart);

        // add user
        Role adminRole = new Role("admin");
        Role userRole = new Role("user");
        Role userRole1 = new Role("user");
        Role userRole2 = new Role("admin");
        Role userRole3 = new Role("admin");
        Role userRole4 = new Role("Administrator");



        User admin = new User();
        admin.setName("Tony");
        admin.setUsername("admin");
        admin.setEmail("admin@yahoo.com");
        admin.setPassword("1");
        Set<Role> roles = new HashSet<>();
        roles.add(adminRole);
        admin.setRoles(roles);

        User user = new User();
        user.setName("Mr.Scott");
        user.setUsername("user");
        user.setEmail("user@yahoo.com");
        user.setPassword("1");
        Set<Role> roles2 = new HashSet<>();
        roles2.add(userRole);
        user.setRoles(roles2);

        User user1 = new User();
        user1.setName("Nutdanai");
        user1.setUsername("Nutdanai");
        user1.setEmail("tanawan.ft@gmail.com");
        user1.setPassword("552115031");
        Set<Role> roles01 = new HashSet<>();
        roles01.add(userRole1);
        user1.setRoles(roles01);

        User user2 = new User();
        user2.setName("Tanawan");
        user2.setUsername("Tanawan");
        user2.setEmail("kamibzdo@gmail.com");
        user2.setPassword("552115023");
        Set<Role> roles02 = new HashSet<>();
        roles02.add(userRole2);
        user2.setRoles(roles02);

        User user3 = new User();
        user3.setName("Kachapon");
        user3.setUsername("Kachapon");
        user3.setEmail("kachapon-21@hotmail.com");
        user3.setPassword("552115000");
        Set<Role> roles03 = new HashSet<>();
        roles03.add(userRole3);
        user3.setRoles(roles03);

        User user4 = new User();
        user4.setName("Admin123");
        user4.setUsername("Admin123");
        user4.setEmail("Adminorp@gmail.com");
        user4.setPassword("77951100");
        Set<Role> roles04 = new HashSet<>();
        roles04.add(userRole4);
        user4.setRoles(roles04);


        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);

        userRepository.save(user);
        userRepository.save(admin);
    }
}
